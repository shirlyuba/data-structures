#include<stdio.h>
#include<malloc.h>


int Check(int* arr, int count)
{
	int i;
	for (i = 0; i < count-1; i++)
	{
		if (arr[i] < arr[i + 1])
		{
			return 1;
		};
	};
	return 0;
}


int* FindIndexesOfSortedMassiv(int* arr, int count)
{
	int* growth = (int*)malloc(count * sizeof(int));
	growth[0] = 0;
	int numberIncreasingElements = 0;
	int i;
	for(i = 1; i < count; i++)
	{
		if (arr[i-1] <= arr[i])
		{
			numberIncreasingElements += 1;
			growth[i] = numberIncreasingElements;
		}
		else
		{
		growth[i] = 0;
		numberIncreasingElements = 0;
		}
	}
	int MaxNumberOfIncreasingElements = growth[0];
	int j;
	for(i = 0; i < count; i++)
	{
		if (growth[i] > MaxNumberOfIncreasingElements)
		{
			MaxNumberOfIncreasingElements = growth[i];
			j = i;
		}
	}
	i = j - MaxNumberOfIncreasingElements;
	int* index = (int*)malloc(2 * sizeof(int));
	index[0] = i;
	index[1] = j;
	return index;
}


int main()
{
	printf("Enter the number of items and their values:\n");
	int count;
	scanf("%d" ,&count);
	int *arr = (int*)malloc(count * sizeof(int));
	int i;
	for(i = 0; i < count; i++)
	{
		scanf("%d", &arr[i]);
	}
	if (Check(arr, count) == 0)
	{
		printf("indexes of sorted massiv: index i = 0 index j = 0 \n");
	}
	else
	{
	int* index = (int*)malloc(2 * sizeof(int));
	index = FindIndexesOfSortedMassiv(arr, count);
	printf("indexes of sorted massiv: index i = %d index j = %d\n", index[0], index[1]);
	free(index);
	}
	free(arr);
	return 0;
}
