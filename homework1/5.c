#include<stdio.h>
#include<malloc.h>


void PrintByte(char x)
{
	int i;
	for(i = 7; i >= 0; i--)
	{
		if (x & (1 << i))
		{
			printf("1");
		}
		else
		{
			printf("0");
		}
	 }
	printf(" ");
}


void BitwiseRepresentation(void* data, int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		PrintByte(*((char*)data + i));
	}
}


int main()
{
	printf("choose type which you want to test: \n");
	printf(" 1 - int\n");
	printf(" 2 - float\n");
	printf(" 3 - double\n");
	printf(" 4 - unsigned int\n");
	int choice;
	scanf("%d", &choice);
	printf("enter the value of item: ");
	switch (choice)
	{
	case 1:
		{
			int sim;
			scanf("%d" , &sim);
			BitwiseRepresentation(&sim, sizeof(int));
			break;
		};
	case 2:
		{
			float sim;
			scanf("%f" , &sim);
			BitwiseRepresentation(&sim, sizeof(float));
			break;
		};
	case 3:
		{
			double sim;
			scanf("%lf" , &sim);
			BitwiseRepresentation(&sim, sizeof(double));
			break;
		};
	case 4:
		{
			unsigned int sim;
			scanf("%u" , &sim);
			BitwiseRepresentation(&sim, sizeof(unsigned int));
			break;
		};
	default:
		printf("your choice is wrong");
	};
	return 0;
}



