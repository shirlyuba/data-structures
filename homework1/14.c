#include<stdio.h>
#include<math.h>


double f1(double x)
{
	return x * x * x - 3 * x * x + 12 * x - 13;
}


double f2(double x)
{
	return x;
}


double f3(double x)
{
	return 1 / (x * x + 2);
}


int binsearch_solve(double(*f)(double), double a, double b, double* x, double epsilon, unsigned max_it)
{
	while ((fabs(f((a+b)/2)) >= epsilon) && (--max_it > 0))
	{
		if (f((a + b) / 2) > 0) 
		{
			b = (a + b)/2;
		}
		else 
		{
			a = (a + b)/2;
		} 
	}
	if (fabs(f((a+b)/2)) < epsilon) 
	{
		*x = (a+b)/2;
		return 1;
	}
	else
	{
		return 0;
	}
}


int main()
{
	double a, b, epsilon, x;
	unsigned max_it;
	printf("Enter borders of segment:\n");
	scanf("%lf %lf", &a, &b);
	printf("address for the result:\n");
	scanf("%lf", &x);
	printf("epsilon :\n");
	scanf("%lf", &epsilon);
	printf("maximum number of iterations: \n");
	scanf("%d", &max_it);
	if (binsearch_solve(f2, a, b, &x, epsilon, max_it))
	{
		printf("yes, solution is found, x = %g", x);
	}
	else
	{
		printf("no, solution is not found");
	}
	printf("\n");
	return 0;
}
