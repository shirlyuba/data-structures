#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct
{
	int x, y;
} point;

float getDistance (point a, point b)
{
	return ((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
}

void bubbleSort(int size, point* arr, point p)
{
	int i, j;
	for ( i = 0; i < size; i++)
	{
		for ( j = 0; j < 1; j++)
		{
			if (getDistance(p, arr[i]) < getDistance(p, arr[j]))
			{
				point tmp = arr[j];
				arr[j] = arr[i];
				arr[i] = tmp;
			}
		}
	}
}

void getNearestPoints(point* arr, int size, int index, int count)
{
	point* buff = (point*)malloc(sizeof(point) * size);
	memcpy(buff, arr, sizeof(point) * size);
	bubbleSort(size, buff, buff[index]);
	printf("Points closed to (%d, %d):\n", arr[index].x, arr[index].y);
	int i;
	for (i = 0; i < count; i++)
	{
		printf("(%d, %d)\n", buff[i].x, buff[i].y);
	}
	free(buff);
}

int main ()
{
	int n;
	scanf("%d", &n);
	point *arr = (point*)malloc(sizeof(point) * n);
	int i;
	for (i = 0; i < n; i++)
	{
		scanf("%d %d", &arr[i].x, &arr[i].y);
	};
	int m = 0;
	scanf("%d", &m);
	for (i = 0; i < m; i++)
	{
		int index, count;
		scanf("%d %d", &index, &count);
		getNearestPoints(arr, n, index, count);
	}
	free(arr);
	return 0;
}
