#include<stdio.h>
#include<malloc.h>


double KohanSum(double* arr, int count)
{
	int i;
	double sum = 0;
	int c = 0;
	int t, y;
    for (i = 0; i < count; i++)
	{
		y = arr[i] - c;
		t = sum + y;
		c = (t-sum)-y;
		sum = t;
	}
	return sum;
}


double Sum(double* arr, int count)
{
	int i;
	double sum = 0;
	for (i = 0; i < count; i++)
	{
		sum = sum + arr[i];
	}
	return sum;
}


int main()
{
	printf("Enter the number of items and their values:\n");
	int count;
	scanf("%d", &count);
	double *arr = (double*)malloc(count*sizeof(double));
	int i;
	for (i = 0;i < count; i++)
	{
		scanf("%lf", &arr[i]);
	}
	printf("Kohan sum: %lf\n", KohanSum(arr, count));
	printf("Standart sum: %lf\n", Sum(arr, count));
	free(arr);
	return 0;
}

