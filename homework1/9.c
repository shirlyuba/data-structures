#include<stdio.h>


double GetIncreasingDoubleSum(int count)
{
	double increasingDoubleSum = 0;
	double i;
	for (i = 1; i <= count; i++)
	{
		increasingDoubleSum += 1/(i*i);
	}
	return increasingDoubleSum;
}


float GetIncreasingFloatSum(int count)
{
	float increasingFloatSum = 0;
	double i;
	for (i = 1; i <= count; i++)
	{
		increasingFloatSum += 1/(i*i);
	}
	return increasingFloatSum;
}


double GetDecreasingDoubleSum(int count)
{
	double decreasingDoubleSum = 0;
	double i;
	for (i = count; i >= 1; i--)
	{
		decreasingDoubleSum += 1/(i*i);
	}
	return decreasingDoubleSum;
}


float GetDecreasingFloatSum(int count)
{
	float decreasingFloatSum = 0;
	double i;
	for (i = count; i >= 1; i--)
	{
		decreasingFloatSum += 1/(i*i);
	}
	return decreasingFloatSum;
}


int main()
{
	printf("Enter the count of elements:\n");
	double count;
	scanf("%lf", &count);
	double pi = 3.1415926;
	printf("real sum %.20lf\n", pi*pi/6);
	printf("increasing double sum: %.20lf\n", GetIncreasingDoubleSum(count));
	printf("increasing float sum : %.20f\n", GetIncreasingFloatSum(count));
	printf("decreasing double sum : %.20lf\n", GetDecreasingDoubleSum(count));
	printf("decreasing float sum : %.20f\n", GetDecreasingFloatSum(count));
	return 0;
}
