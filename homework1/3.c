#include<stdio.h>
#include<stdlib.h>


void mem_rotr(void* p, unsigned size, unsigned k);
void RotateRight(char* a, unsigned size, unsigned k);
void ReverseOrder(char* a, unsigned length);
void BinPrintChar(char x);


int main()
{
	unsigned size, k, i;
	printf("Enter the size of the memory and the number of bits, which you want to shift it: ");
	scanf("%u %u", &size, &k);
	char* p = (char*)malloc(size);
	printf("Enter the data: ");
	for (i = 0; i < size; ++i)
	{
		unsigned x;
		scanf("%u", &x);
		p[i] = (char)x;
	}
	printf("bit representation before the shift: \n");
	for (i = 0; i < size; ++i)
	{
		BinPrintChar(p[i]);
	}
	printf("\n");
	printf("bit representation after the shift: \n");
	mem_rotr(p, size, k);
	for (i = 0; i < size; ++i)
	{
		BinPrintChar(p[i]);
	}
	printf("\n");
	free(p);
	return 0;
}


void mem_rotr(void* p, unsigned size, unsigned k)
{
	unsigned byte_shift = k / 8;
	unsigned bit_shift = k % 8;
	char shift = 0;
	char tmp;
	char* q = (char*)p;
	char buffer[2];
	RotateRight(q, size, byte_shift);
	int i;
	for (i = 0; i < bit_shift; ++i)
	{
		shift = shift | (1 << i);
	}
	buffer[0] = (q[0] & shift) << (8 - bit_shift);
	q[0] = q[0] >> bit_shift;
	for (i = 1; i < size; ++i)
	{
		buffer[i % 2] = (q[i] & shift) << (8 - bit_shift);
		q[i] = q[i] >> bit_shift;
		q[i] = q[i] | buffer[(i + 1) % 2];
	}
	q[0] |= buffer[(size - 1) % 2];
}


void RotateRight(char* a, unsigned size, unsigned k)
{
	ReverseOrder(a, size);
	ReverseOrder(a, k);
	ReverseOrder(&a[k], size - k);
}


void ReverseOrder(char* a, unsigned length)
{
	char i, tmp;
	for (i = 0; i < length / 2; ++i)
	{
		tmp = a[i];
		a[i] = a[(length - 1) - i];
		a[(length - 1) - i] = tmp;
	}
}


void BinPrintChar(char x)
{
	 int i;
	 for(i = 7; i >= 0; i--)
	 {
	     if (x & (1 << i))
	     {
	         printf("1");
	     }
	     else
         {
             printf("0");
         }
	 }
	 printf(" ");
	 return ;
}

