#include<stdio.h>
#include<stdlib.h>


void swap(int* a, int* b)
{
	int c = *a;
	*a = *b;
	*b = c;
}


int partition(int* a, int l, int r)
{
	int pivot = rand();
	swap(&pivot, &a[r]);
	int i = l - 1;
	int j = r;
	while (i < j)
	{
		while ((a[++i] < pivot) && (i < r));
		while ((j > i) && (a[--j] >= pivot));
		if (j <= i)
		{
			break;
		}
		swap(&a[i], &a[j]);
	}
	swap(&pivot, &a[r]);
	swap(&a[i], &a[r]);
	return i;
}


int FindMiddleOrderStat(int* a,int l, int r, int k)
{
	if (r == l)
	{
		return a[r];
	}
	int mid = partition(a, l, r);
	if (k == mid - l)
	{
		return a[mid];
	}
	if (mid - l > k)
	{
		return FindMiddleOrderStat(a, l, mid - 1, k);
	}
	else
	{
		return FindMiddleOrderStat(a, mid + 1, r, k - mid - 1);
	}
}


int main()
{
	int n;
	printf ("Enter the count of elements and their values: ");
	scanf ("%d", &n);
	int* arr = (int*)malloc(n * sizeof(int));
	for (int i = 0; i < n; i++)
	{
		scanf("%d", &arr[i]);
	}
	int c = FindMiddleOrderStat(arr, 0, n - 1, n/2 );
	printf ("\nthe median is %d \n", c);
	free(arr);
	return 0;
}