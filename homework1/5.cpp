#include<stdio.h>
#include<malloc.h>
void PrintByte(int c)
 {  
	 int i;
	 for(i = 7; i >= 0; i--)
	 {
          if (c & (1 << i))
               printf("1");
	      else printf("0");
	 }
	 return ;
 }
void BitwiseRepresentation(void* data, int size)
{
	int i;
    for(i = 1; i < size; i++)
		PrintByte (*((int*)data+i));
    return ;
}
int main()
{
	double sim;
	scanf("%lf" , &sim);
    BitwiseRepresentation(&sim, sizeof(double));
	return 0;
}
