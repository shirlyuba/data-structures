#include<stdio.h>
#include<malloc.h>
#include<math.h>


typedef struct
{
	float x;
	float y;
} point;


float GetDistance(point a, point b)
{
	return (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y);
}


float FindDiametr(point* arr, int count)
{
	float distance = 0;
	int i, j;
	float d;
	for (i = 0; i < count; i++)
	{
		for (j = 0; j < count; j++)
		{
			d = GetDistance(arr[i], arr[j]);
			if (d > distance)
			{
				distance = d;
			}
		}
	}
	return sqrt(distance);
}


int main()
{
	int count;
	printf("enter the count of points: ");
	scanf("%d", &count);
	printf("enter the points in format (x,y): \n");
	int i;
	point* arr = (point*)malloc(count * sizeof(point));
	for (i = 0; i < count; i++)
	{
		scanf("%f %f", &arr[i].x, &arr[i].y);
	}
	printf("the diametr is : ");
	printf("%g\n", FindDiametr(arr, count));
	free(arr);
	return 0;
}
