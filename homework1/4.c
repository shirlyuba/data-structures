#include<stdio.h>
#include<stdlib.h>


void CreateMatrix(int** arr, int n)
{
	int i, j;
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
			arr[i][j] = 0;
	}
	arr[0][0] = 1;
	int k = 1;
	i = 0;
	j = 1;
	while (k < n * n)
	{
		k = k + 1;
		arr[i][j] = k;
		if ((j > 0) && (arr[i][j-1] == 0))
			j--;
		else if ((i > 0) && (arr[i-1][j] == 0))
			i--;
		else if (((j == 0) && (arr[i][j+1] > 0)) || ((j % 2 == 1) && (arr[i][j-1] > 0) && ((i == 0) || ((i > 0) && (arr[i][j] > 1 + arr[i][j-1])))))
			i++;
		else j++;
	}
	for(i = 0; i < n; i++)
	{ 
		for(j = 0; j < n; j++)
		{
			printf("%6d  ", arr[i][j]);
		};
		printf("\n");
	}
}


int main()
{
	int n;
	printf("enter the size of the matrix:\n");
	scanf("%d",&n);
	int** arr = (int**)malloc(n * n * sizeof(int*));
	int i;
	for ( i = 0; i < n; i++)
		arr[i] = (int*)malloc(n * sizeof(int));
	CreateMatrix(arr, n);
	printf("\n");
	for ( i = 0; i < n; i++)
		free(arr[i]);
	free(arr);
	return 0;
}