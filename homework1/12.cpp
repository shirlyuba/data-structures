#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
void swap(int*a, int*b)
{
	int c=*a;
	*a=*b;
	*b=c;
}

void bubbleSort(int*a, int count)
{
	 int i,j;
     for ( i = count-1;  i >= 0; i--)
		  for ( j = 0; j < i; j++)
			  if (a[j] > a[j+1])
				  swap(&a[j],&a[j+1]);
}
void randomGeneration(int* arr, int count)
{    
	srand(78);
	int i;
	for(i = 0; i < count; i++)
	arr[i] = rand();
}
void inwriteToConsole(int* arr,int count)
{ 
	int i;
	for(i = 0; i < count; i++)
		scanf("%d", &arr[i]);
}
void readFromFile(int* arr, int count)
{
	FILE* in;
	int i;
    in = fopen("random.txt", "r");
    for (i = 0; i < count; i++)
	fscanf(in, "%d", &arr[i] );
    fclose(in);
}
void writeToFile(int* arr, int count)
{
	FILE* out;
    out = fopen("sortedMassiv.txt", "w");
	int i;
    for (i = 0; i < count; i++)
		if (out==NULL)
			printf("file is not opened");
	for (i = 0; i < count; i++)
		fprintf(out, "%d " , arr[i]);
	fclose(out);
}
void outwriteToConsole(int*arr, int count)
{
	int i;
	for (i = 0;i < count; i++)
		printf("%d ", arr[i]);
}
int rightSort(int* arr, int count)
{
    int i;
    for(i = 0; i < count; i++)
		if (arr[i] < arr[i+1])
			 return 1;
		else return 0;

}		
int main()
{   
	int count;
	printf("write the count of elements:\n");
	scanf("%d", &count);
	printf("\n");
    int * arr = (int*)malloc(count * sizeof(int));
	int way;
	printf("Write 1 - for random massiv;\n write 2 - for handed massiv;\n write 3 - for massiv from file random.txt\n");
	scanf("%d", &way);
	switch (way)
	{
	case 1: randomGeneration(arr,count);
		    break;
	case 2: inwriteToConsole(arr,count);
		    break;
	default:
		    readFromFile(arr,count);
	}
	bubbleSort(arr, count);
	if (rightSort(arr, count))
		printf("massiv otsortirovan verno\n");
	int outing;
	printf("Write 1 - for write massiv in file sortedMassiv.txt;\n write 2 - for write massiv to console\n");
	scanf("%d", &outing);
	switch (outing)
	{
	case 1: writeToFile(arr,count);
		    break;
	default:
		    outwriteToConsole(arr,count);
	};
	free(arr);
	return 0;
}

