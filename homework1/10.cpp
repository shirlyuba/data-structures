#include<stdio.h>
#include<malloc.h>
int main()
{ 
	int count;
	printf("Enter the number of items and their values: ");
	scanf("%d", &count);
    float * arr = (float*)malloc(count*sizeof(float));
	int i;
	for (i = 0; i < count; i++)
		scanf("%f", &arr[i]);
	float min = arr[0];
	float max = arr[0];
	for (i = 1; i < count; i++)
	{
		if (max < arr[i])
			max = arr[i];
	    if (min > arr[i])
			min = arr[i];
	}
	printf("%g\n", max-min);
	free(arr);
	return 0;
}

