#include<stdio.h>
#include<malloc.h>

float* getFloatArr(int power)
{
	float* arr = (float*)malloc((power + 1) * sizeof(float));
	int i;
	for (i = 0; i < power + 1; i++)
	{
		scanf("%f", &arr[i]);
	}
	return arr;
}

float sum(float* arr, float x, int power)
{
	int i;
	float sum = arr[0];
	for (i = 1; i < power + 1; i++)
	{
		sum = x * sum + arr[i];
	}
	return sum;
}

int main()
{
	printf("enter the value of x:\n");
	float x;
	scanf("%f", &x);
	int power;
	printf("enter the degree of the polynomial:\n");
	scanf("%d", &power);
	printf("enter the coefficients of x from the highest degree to the lowest:\n");
	float* arr = getFloatArr(power);
	printf("the value of a polynomial: %g\n", sum(arr, x, power));
	free(arr);
	return 0;
}


