#include<stdio.h>
#include<malloc.h>


int* FindIndexesOfBestSumm(int* a, int count)
{
	int best_i = 0;
	int best_j = 0;
	int best_sum = 0;
	int i, j, k;
	for (i = 0; i < count; i++)
	{
		for (j = i + 1; j < count; j++)
		{
		int sum = 0;
		int k;
		for (k = i; k <= j; k++)
		{
			sum += a[k];
		}
		if (best_sum < s)
		{
			best_i = i;
			best_j = j;
			best_sum = s;
		}
		}
	}
	int* index = (int*)malloc(2 * sizeof(int));
	index[0] = best_i;
	index[1] = best_j;
	return index;
}


int main()
{
	printf("Enter the number of elements and their values:\n");
	int count;
	scanf("%d", &count);
	int *arr = (int*)malloc(count * sizeof(int));
	for (int i = 0; i < count; i++)
		scanf("%d", &arr[i]);
	int* index = (int*)malloc(2 * sizeof(int));
	index = FindIndexesOfBestSumm(arr, count);
	printf("i - %d, j - %d", index[0], index[1]);
	free(arr);
	free(index);
	return 0;
}