#include<stdio.h>
#include<malloc.h>
#include<math.h>
int main()
{ 
	int n;
    printf("Enter the number of items and their values:");
    scanf("%d", &n);
    float * arr = (float*)malloc(n * sizeof(float));
    int i;
    for (i = 0; i < n; i++)
        scanf("%f", &arr[i]);
    float sum = 0;
    for (i = 0; i < n; i++)
        sum = sum + arr[i];
    sum = sum/n;
    float min = fabs(sum-arr[0]);
    float minx = arr[0];
    for (i = 1; i < n; i++)
        if (min > fabs(sum-arr[i]))
        {
            min = fabs(sum-arr[i]);
            minx = arr[i];
        }
    printf("%g", minx);
    printf("\n");
	free(arr);
	return 0;
}
