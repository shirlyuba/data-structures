#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<time.h>


void swap(int*a, int*b)
{
	int c=*a;
	*a=*b;
	*b=c;
}


void bubbleSort(int*a, int count)
{
	 int i,j;
     for ( i = count - 1;  i >= 0; i--)
	 {
		  for ( j = 0; j < i; j++)
		  {
			  if (a[j] > a[j+1])
			  {
				  swap(&a[j],&a[j+1]);
			  }
		  }
	 }
}


void GetMassInt(int* a,int n)
{
	int i;
	srand(time(NULL));
	for (i = 0; i < n; i++)
	{
		a[i] = rand();
	}
}


void inwriteToConsole(int* arr,int count)
{
	int i;
	for(i = 0; i < count; i++)
	{
		scanf("%d", &arr[i]);
	}
}


void readFromFile(char* nameOfFile, int* arr, int count)
{
	FILE* in;
	in = fopen(nameOfFile, "r");
	if (in == NULL)
	{
		printf("file is not opened");
	}
	int i;
	for (i = 0; i < count; i++)
	{
		fscanf(in, "%d", &arr[i] );
	}
	fclose(in);
}


void writeToFile(char* nameOfFile, int* arr, int count)
{
	FILE* out;
	out = fopen(nameOfFile, "w");
	int i;
	if (out == NULL)
	{
		printf("file is not opened");
	}
	for (i = 0; i < count; i++)
	{
		fprintf(out, "%d " , arr[i]);
	}
	fclose(out);
}


void PrintMassInt(int* arr, int count)
{
	int i;
	for (i = 0; i < count; i++)
	{
		printf("%d ", arr[i]);
	}
}


int check(int* arr, int count)
{
	int i;
	for(i = 0; i < count - 1; i++)
	{
		if (arr[i] > arr[i+1])
		{
			return 0;
		}
	}
	return 1;
}


int main()
{
	int count;
	printf("write the count of elements:\n");
	scanf("%d", &count);
	printf("\n");
	int* arr = (int*)malloc(count * sizeof(int));
	printf("write 1 - to create random array;\n");
	printf("write 2 - to enter array in console;\n");
	printf("write 3 - to read array from file array.txt\n");
	int input;
	scanf("%d", &input);
	switch (input)
	{
	case 1:
		{
			GetMassInt(arr, count);
			break;
		}
	case 2:
		{
			inwriteToConsole(arr, count);
			break;
		}
	default:
		{
			readFromFile("array.txt", arr, count);
		}
	}
	bubbleSort(arr, count);
	if (check(arr, count))
	{
		printf("array was sorted right\n");
	}
	int output;
	printf("write 1 - to write array in file sortedArray.txt;\n");
	printf("write 2 - to write array in console\n");
	scanf("%d", &output);
	switch (output)
	{
	case 1:
		{
			writeToFile("sortedArray.txt", arr, count);
			break;
		}
	default:
		{
			PrintMassInt(arr, count);
		}
	}
	free(arr);
	return 0;
}
