#include <stdio.h>
#include <malloc.h>
void mem_rotr(void *arr, int size, int k)
{
	int byte = k / 8;
	int bit = k % 8, i;
	char sdvig = 0;
	char *q = (char *)arr;
	char mem[2];
	for (i = 0; i < bit; ++i)
	{
		sdvig = sdvig | (1 << i);
	}
	mem[0] = (q[0] & sdvig) << (8 - bit);
	q[0] = q[0] >> bit;
	for (i = 1; i < size; ++i)
	{
		mem[i % 2] = (q[i] & sdvig) << (8 - bit);
		q[i] = q[i] >> bit;
		q[i] = q[i] | mem[(i + 1) % 2];
	}
	q[0] |= mem[(size - 1) % 2];
}

int main()
{
	int size, k, i;
	scanf("%d %d", &size, &k);
	char *arr = (char *)malloc(size*sizeof(char));
	for (i = 0; i < size; ++i)
	{
		int x;
		scanf("%d", &x);
		arr[i] = (char) x;
	}
	mem_rotr(arr, size, k);
	for (i = 0; i < size; ++i)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	free(arr);
	return 0;
}
