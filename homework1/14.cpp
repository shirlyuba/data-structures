#include<stdio.h>
#include<math.h>
double f1(double x)
{
	return x*x*x - 3*x*x + 12*x - 13;
}
double f2(double x)
{
	return x;
}
double f3(double x)
{
	return 1 / (x*x + 2);
}
int binsearch_solve(double(*f)(double), double a, double b, double *x, double epsilon, unsigned max_it)
{
	unsigned iter = 0;
	while (iter <= max_it)
	{
          if ((*f)(*x) > (*f)((a + b) / 2))
			  a = a/2;
		  else b = b/2;
		  if (fabs((*f)(*x))< epsilon)
		  {
			  return 1;
			  break;
		  }
         iter = iter + 1;
	}
return 0;
}
	int main()
	{
		double a, b, epsilon, x;
	    unsigned max_it;
		scanf("%f %f %f %f %d", &a, &b, &x, &epsilon, &max_it);
        if (binsearch_solve(&f2, a, b, &x, epsilon, max_it))
			printf("yes ");
		else printf("no ");
		printf("\n");
		return 0;
	}
