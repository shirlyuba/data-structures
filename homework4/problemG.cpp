#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

class point
{
public:
	bool operator==(const point& point1)
	{
		return (x == point1.x && y == point1.y);
	};
	long double x, y;
};

long double det(const point & point1, const point & point2, const point & point3)
{
	long double a = point2.x - point1.x;
	long double b = point2.y - point1.y;
	long double c = point3.x - point1.x;
	long double d = point3.y - point1.y;
	return a*d - b*c;
}

long double baseOfTriangle(const point & point1, const point & point2)
{
	return sqrt(pow((point1.x - point2.x), 2) + pow((point1.y - point2.y), 2));
}

long double distance(const point & point1, const point & point2, const point & point3)
{
	return  fabs(det(point1, point2, point3) / baseOfTriangle(point2, point3));
}

long double route(const point & point1, const point & point2)
{
	if (fabs(point1.x - point2.x) > fabs(point1.y - point2.y))
	{
		return fabs(point1.y - point2.y) * (sqrt(2) - 1) + fabs(point1.x - point2.x);
	}
	else
	{
		return fabs(point1.x - point2.x) * (sqrt(2) - 1) + fabs(point1.y - point2.y);
	}
}

bool cmp(const point & point1, const point & point2)
{
	return point1.x < point2.x || point1.x == point2.x && point1.y < point2.y;
}

void convexHull(vector <point> & points)
{
	if (points.size() < 3)
	{
		return;
	}
	sort(points.begin(), points.end(), &cmp);
	point p1 = points[0];
	point p2 = points[points.size() - 1];
	vector <point> up, down;
	up.push_back(p1);
	down.push_back(p1);
	for (int i = 1; i < points.size(); i++)
	{
		if (i == points.size() - 1 || (det(p1, points[i], p2) < 0))
		{
			while (up.size() >= 2 && (det(up[up.size() - 2], up[up.size() - 1], points[i]) >= 0))
				up.pop_back();
			up.push_back(points[i]);
		}
		if (i == points.size() - 1 || (det(p1, points[i], p2) > 0))
		{
			while (down.size() >= 2 && (det(down[down.size() - 2], down[down.size() - 1], points[i])) <= 0)
				down.pop_back();
			down.push_back(points[i]);
		}
	}
	points.clear();
	for (int i = 0; i < up.size(); i++)
		points.push_back(up[i]);
	for (int i = down.size() - 2; i > 0; i--)
		points.push_back(down[i]);
}

point next(const vector <point> & points, const point & B)
{
	for (int i = 0; i < points.size(); i++)
	{
		if ((points[i].x == B.x) && (points[i].y == B.y))
		{
			if (i == points.size() - 1)
				return points[0];
			else
			{
				return points[i + 1];
			}
		}
	}
}

pair <point, point> findSolution(vector <point> & points)
{
	point A;
	point B;
	A = points[0];
	int i = 2;
	double max = 0;
	while ((i != points.size()) && (distance(points[i], points[1], points[0]) > max))
	{
		max = distance(points[i], points[1], points[0]);
		i++;
	}
	B = points[i - 1];
	point start;
	point start1;
	if (!(B == points[points.size() - 1]))
	{
		if (route(A, B) > route(next(points, A), B))
		{
			start = A;
		}
		else
		{
			start = next(points, A);
		}
		if (route(A, (next(points, B))) > route(next(points, A), next(points, B)))
		{
			start1 = A;
		}
		else
		{
			start1 = next(points, A);
		}
		if (route(start, B) < route(start1, next(points, B)))
		{
			B = next(points, B);
		}
	}
	start = A;
	point finish = B;
	double answer = route(start, finish);
	for (int i = 1; i < points.size() - 1; i++)
	{
		A = next(points, A);
		while (distance(B, next(points, A), A) < distance(next(points, B), next(points, A), A))
		{
			B = next(points, B);
		}
		if (answer < route(A, B))
		{
			answer = route(A, B);
			start = A;
			finish = B;
		}
		if (answer < route(next(points, A), B))
		{
			answer = route(next(points, A), B);
			start = next(points, A);
		}
	}
	return{ start, finish };
}

int main()
{
	int N;
	cin >> N;
	vector <point> points;
	vector <point> original;
	for (int i = 0; i < N; i++)
	{
		point pres;
		cin >> pres.x >> pres.y;
		points.push_back(pres);
		original.push_back(pres);
	}
	convexHull(points);
	/*for (int i = 0; i < points.size(); i++)
	{
	cout << points[i].x << " " << points[i].y << endl;
	}*/
	double max = 0;
	auto ans = findSolution(points);
	point firstPoint = ans.first;
	point secondPoint = ans.second;
	for (int i = 0; i < original.size(); i++)
	{
		if ((original[i].x == firstPoint.x) && (original[i].y == firstPoint.y))
		{
			cout << i + 1 << " ";
		}
		if ((original[i].x == secondPoint.x) && (original[i].y == secondPoint.y))
		{
			cout << i + 1 << " ";
		}
	}
	return 0;
}