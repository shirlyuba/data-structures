#include <iostream>
#include <vector>
#include <iomanip>
#include <string>
#include <algorithm>


#define BASE 10

class BigInt
{
public:
	BigInt();
	BigInt(const BigInt & bint);
	BigInt(int num);
	friend std::ostream & operator<< (std::ostream & os, const BigInt & bint);
	void operator=(const BigInt & bint);
	BigInt operator-() const;
	BigInt operator+ (const BigInt & bint) const;
	friend BigInt operator-(const BigInt & a, const BigInt & b);
	friend BigInt operator*(const BigInt & a, const BigInt & b);
	friend bool operator>= (const BigInt& a, const BigInt& b);
	friend bool operator< (const BigInt& a, const BigInt& b);

private:
	std::vector<char> _arr;
	int _size;
	char _sign;
};


template <typename T>
class Matrix
{
	friend std::ostream& operator <<(std::ostream& out, const Matrix& matrix)
	{
		for (int i = 0; i < matrix.n; i++)
		{
			for (int j = 0; j < matrix.m; j++)
			{
				std::cout << std::setw(20);
				std::cout << matrix[i][j];
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
		return out;
	};
	friend std::istream &operator>>(std::istream & in, Matrix & matrix)
	{
		for (int i = 0; i < matrix.n; i++)
			for (int j = 0; j < matrix.m; j++)
				std::cin >> matrix[i][j];
		return in;
	};
public:
	Matrix(const int &, const int &);
	~Matrix();
	std::vector <T> & operator[](const int &);
	const std::vector <T> operator[] (const int &) const;
	Matrix operator* (const Matrix &);
	Matrix operator*= (const Matrix &);
	Matrix powMat(int);
	int n;
	int m;
private:
	std::vector <std::vector <T> > matr;
};

BigInt::BigInt()
{
	_size = 0;
	_sign = 1;
	_arr.assign(0, 0);
}
BigInt::BigInt(int num)
{
	_size = 0;
	if (num >= 0)
		_sign = 1;
	else {
		_sign = -1;
		num *= -1;
	}
	_size = 0;
	while (num != 0)
	{
		_arr.push_back(num % BASE);
		num /= BASE;
		++_size;
	}
}



BigInt::BigInt(const BigInt & bint) : _size(bint._size), _sign(bint._sign), _arr(bint._arr) {}

void BigInt::operator= (const BigInt & bint)
{
	_size = bint._size;
	_sign = bint._sign;
	_arr = bint._arr;
}

BigInt BigInt::operator-() const
{
	BigInt res;
	res._arr = _arr;
	res._sign = -1 * _sign;
	res._size = _size;
	return res;
}

std::ostream & operator<<(std::ostream & os, const BigInt & bint)
{
	if (bint._size == 0) {
		std::cout << "0";
		return os;
	}
	if (bint._sign < 0)
		std::cout << "-";
	for (int i = bint._size - 1; i >= 0; --i)
		std::cout << (int)bint._arr[i];
	return os;
}


BigInt BigInt::operator+(const BigInt & bint) const
{
	BigInt res;
	if (_sign * bint._sign == 1)
	{
		res._sign = _sign;
		res._size = (_size > bint._size) ? _size : bint._size;
		res._arr.assign(res._size, 0);
		int rest = 0;
		for (int i = 0; i < res._size; ++i)
		{
			char comp1 = (_size > i) ? _arr[i] : 0;
			char comp2 = (bint._size > i) ? bint._arr[i] : 0;
			res._arr[i] = (comp1 + comp2 + rest) % BASE;
			rest = (comp1 + comp2 + rest) / (BASE);
		}
		if (rest == 1) {
			res._arr.push_back(1);
			++res._size;
		}
		return res;
	}
	if (bint._sign == -1)
	{
		return *this - (-bint);
	}
	if (this->_sign == -1)
		return -(bint - (-*this));
}

BigInt operator-(const BigInt & a, const BigInt & b)
{
	if ((a._sign > 0) && (b._sign < 0))
		return a + (-b);
	if ((a._sign < 0) && (b._sign > 0))
		return -((-a) + b);
	if ((a._sign < 0) && (b._sign < 0))
		return -((-b) - (-a));
	if ((a._sign > 0) && (b._sign > 0))
	{
		if (a < b)
			return -(b - a);
		if (a >= b)
		{
			BigInt res;
			std::vector<char> arr1, arr2;
			arr1 = a._arr;
			arr2 = b._arr;
			for (int i = 0; i < a._size; ++i)
			{
				char comp2 = (b._size > i) ? arr2[i] : 0;
				if (arr1[i] < comp2)
				{
					int k = i + 1;
					while (arr1[k] == 0) ++k;
					--arr1[k];
					for (int j = k - 1; j > i; --j)
						arr1[j] += BASE - 1;
					arr1[i] += BASE;
				}
				res._arr.push_back(arr1[i] - comp2);
				++res._size;
			}
			int k = res._size - 1;
			while ((res._arr[k] == 0) && (k > 0))
			{
				res._arr.erase(res._arr.end() - 1);
				--res._size;
				--k;
			}
			return res;
		}
	}
}

bool operator>=(const BigInt & a, const BigInt & b)
{
	if ((a._sign > 0) && (b._sign <= 0))
		return 1;
	if ((a._sign <= 0) && (b._sign > 0))
		return 0;
	if ((a._sign > 0) && (b._sign > 0))
	{
		if (a._size > b._size)
			return 1;
		if (a._size < b._size)
			return 0;
		int k = a._size - 1;
		while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
		if (k == -1)
			return 1;
		if (a._arr[k] > b._arr[k])
			return 1;
		else
			return 0;
	}
	if ((a._sign < 0) && (b._sign < 0))
	{
		if (a._size > b._size)
			return 0;
		if (a._size < b._size)
			return 1;
	}
	int k = a._size - 1;
	while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
	if (k == -1)
		return 1;
	if (a._arr[k] < b._arr[k])
		return 1;
	else
		return 0;
}

bool operator<(const BigInt & a, const BigInt & b)
{
	if ((a._sign > 0) && (b._sign < 0))
		return 0;
	if ((a._sign < 0) && (b._sign > 0))
		return 1;
	if ((a._sign > 0) && (b._sign > 0))
	{
		if (a._size > b._size)
			return 0;
		if (a._size < b._size)
			return 1;
		int k = a._size - 1;
		while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
		if (k == -1)
			return 0;
		if (a._arr[k] < b._arr[k])
			return 1;
		else
			return 0;
	}
	if ((a._sign < 0) && (b._sign < 0))
	{
		if (a._size < b._size)
			return 0;
		if (a._size > b._size)
			return 1;
	}
	int k = a._size - 1;
	while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
	if (k == -1)
		return 0;
	if (a._arr[k] > b._arr[k])
		return 1;
	else
		return 0;
}


BigInt operator*(const BigInt & a, const BigInt & b)//Karatsuba
{
	if ((a._size == 0) || (b._size == 0))
		return BigInt(0);
	int ka = 0, kb = 0;
	for (int i = 0; i < a._size; ++i) ka += a._arr[i];
	for (int i = 0; i < b._size; ++i) kb += b._arr[i];
	if (ka == 0 || kb == 0)
		return BigInt(0);
	if ((a._size == 1) && (b._size == 1))
		return BigInt(a._arr[0] * b._arr[0]);
	int divideSize = (a._size > b._size) ? a._size : b._size;
	if (divideSize % 2 == 1)
		++divideSize;
	divideSize /= 2;
	BigInt a1, a2, b1, b2;
	for (int i = 0; i < divideSize; ++i)
	{
		if (a._size > i) {
			a1._arr.push_back(a._arr[i]);
			++a1._size;
		}
		if (b._size > i) {
			b1._arr.push_back(b._arr[i]);
			++b1._size;
		}
		if (a._size > divideSize + i) {
			a2._arr.push_back(a._arr[divideSize + i]);
			++a2._size;
		}
		if (b._size > divideSize + i) {
			b2._arr.push_back(b._arr[divideSize + i]);
			++b2._size;
		}
	}
	BigInt res;
	BigInt comp1, comp2, comp3;
	comp1 = a1 * b1;
	comp2 = ((a1 + a2) * (b1 + b2) - (a1*b1 + a2*b2));
	if (comp2._size != 0) {
		comp2._arr.insert(comp2._arr.begin(), divideSize, 0);
		comp2._size += divideSize;
	}
	comp3 = a2 * b2;
	if (comp3._size != 0) {
		comp3._arr.insert(comp3._arr.begin(), 2 * divideSize, 0);
		comp3._size += divideSize * 2;
	}
	res = comp1 + comp2 + comp3;
	res._sign = a._sign * b._sign;
	return res;
}




template <typename T>
std::vector <T> & Matrix <T>::operator[](const int & index)
{
	return matr[index];
}

template <typename T>
const std::vector <T> Matrix <T>::operator[] (const int & index) const
{
	return matr[index];
}

template <typename T>
Matrix<T>::Matrix(const int & x, const int & y)
{
	n = x;
	m = y;
	while ((m <= 0) || (n <= 0))
	{
		std::cout << "wrong sizez, enter new sizes" << std::endl;
		std::cin >> n >> m;
	}
	matr.reserve(n);
	matr.resize(n);
	for (int i = 0; i < n; i++)
	{
		matr[i].reserve(m);
		matr[i].resize(m);
		matr[i].assign(m, 0);
	}
};

template <typename T>
Matrix<T>::~Matrix()
{
	for (int i = 0; i < n; i++)
	{
		matr[i].clear();
	}
};

template <typename T>
Matrix <T> Matrix <T>::operator*= (const Matrix & matrix)
{
	if (m != matrix.n)
	{
		Matrix <T> Error(1, 1);
		std::cout << "wrong sizes";
		return Error;
	}
	else
	{
		Matrix <T> tmp(n, m);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < matrix.m; j++)
				for (int k = 0; k < m; k++)
					tmp[i][j] = tmp[i][j] + (*this)[i][k] * matrix[k][j];
		matr = tmp.matr;
		return (*this);
	}
}

template <typename T>
Matrix <T> Matrix <T>::operator* (const Matrix & matrix)
{
	if (m != matrix.n)
	{
		Matrix <T> Error(1, 1);
		std::cout << "wrong sizes";
		return Error;
	}
	else
	{
		Matrix <T> result(n, matrix.m);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < matrix.m; j++)
				for (int k = 0; k < m; k++)
					result[i][j] = result[i][j] + (*this)[i][k] * matrix[k][j];
		return result;
	}
};



template <typename T>
Matrix <T> Matrix <T>::powMat(int degree)
{
	Matrix <T> Identity(n, m);
	for (int i = 0; i < n; i++)
	{
		Identity[i].assign(n, 0);
		Identity[i][i] = 1;
	}
	if (degree == 0)
	{
		return Identity;
	}
	Matrix <T> result(n, m);
	Matrix <T> X(n, m);
	X.matr = matr;
	result.matr = Identity.matr;
	int degree1 = degree;
	int count = 1;
	while (degree1 > 1)
	{
		degree1 /= 2;
		count++;
	}
	for (int i = 0; i < count; i++)
	{
		if (degree & (1 << i))
		{
			result *= X;
		}
		X *= X;
	}
	return result;
}

template <typename T>
void createSolutionMatrix(Matrix <T> & matrix, Matrix <T> & ans)
{
	for (int i = 1; i <= 9; i++)
		if (i != 8)
			ans[i][0] = 1;
	matrix[0][4] = 1;
	matrix[0][6] = 1;
	matrix[1][6] = 1;
	matrix[1][8] = 1;
	matrix[2][7] = 1;
	matrix[2][9] = 1;
	matrix[3][4] = 1;
	matrix[3][8] = 1;
	matrix[4][0] = 1;
	matrix[4][3] = 1;
	matrix[4][9] = 1;
	matrix[6][0] = 1;
	matrix[6][1] = 1;
	matrix[6][7] = 1;
	matrix[7][2] = 1;
	matrix[7][6] = 1;
	matrix[8][1] = 1;
	matrix[8][3] = 1;
	matrix[9][2] = 1;
	matrix[9][4] = 1;
}


int main()
{
	int N;
	std::cin >> N;
	if (N > 50)
	{
		BigInt sum = 0;
		Matrix <BigInt> ans(10, 1);
		Matrix <BigInt> matrix(10, 10);
		createSolutionMatrix(matrix, ans);
		ans = matrix.powMat(N - 1) * ans;
		for (int i = 0; i < 10; i++)
			sum = sum + ans[i][0];
		std::cout << sum;
	}
	else
	{
		long long sum = 0;
		Matrix <long long> ans(10, 1);
		Matrix <long long> matrix(10, 10);
		createSolutionMatrix(matrix, ans);
		ans = matrix.powMat(N - 1) * ans;
		for (int i = 0; i < 10; i++)
			sum = sum + ans[i][0];
		std::cout << sum;
	}
	return 0;
}
