#include <iostream>
#include <vector>
#include <cstdlib>
#include <iomanip>

template <typename T>
class Matrix
{
	friend std::ostream& operator <<(std::ostream& out, const Matrix& matrix)
	{
		for (int i = 0; i < matrix.row; i++)
		{
			for (int j = 0; j < matrix.col; j++)
			{
				std::cout << std::setw(12);
				std::cout << matrix[i][j];
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
		return out;
	};
	friend std::istream &operator>>(std::istream & in, Matrix & matrix)
	{
		for (int i = 0; i < matrix.row; i++)
			for (int j = 0; j < matrix.col; j++)
				std::cin >> matrix[i][j];
		return in;
	};
public:
	Matrix(const int &, const int &);
	~Matrix();
	std::pair <int, int> getSize();
	std::vector <T> & operator[](const int &);
	const std::vector <T> operator[] (const int &) const;
	Matrix operator+= (const Matrix &);
	Matrix operator-= (const Matrix &);
	Matrix operator*= (const Matrix &);
	Matrix operator/= (Matrix &);
	Matrix operator+ (const Matrix &);
	Matrix operator- (const Matrix &);
	Matrix operator* (const Matrix &);
	Matrix operator/ (Matrix &);
	Matrix operator* (const T&);
	Matrix operator*= (const T&);
	Matrix powMat(const int &);
	int permutation(Matrix &, const int &);
	long double determinant();
	T track();
	Matrix <long double> inverse();
	Matrix transp();
private:
	int row;
	int col;
	std::vector <std::vector <T> > matr;
};

template <typename T>
std::vector <T> & Matrix <T>::operator[](const int & index)
{
	return matr[index];
}

template <typename T>
const std::vector <T> Matrix <T>::operator[] (const int & index) const
{
	return matr[index];
}

template <typename T>
Matrix<T>::Matrix(const int & new_row, const int & new_col)
{
	row = new_row;
	col = new_col;
	while ((col <= 0) || (row <= 0))
	{
		std::cout << "wrong sizez, enter new sizes" << std::endl;
		std::cin >> row >> col;
	}
	matr.reserve(row);
	matr.resize(row);
	for (int i = 0; i < row; i++)
	{
		matr[i].reserve(col);
		matr[i].resize(col);
	}
};

template <typename T>
Matrix<T>::~Matrix()
{
	for (int i = 0; i < row; i++)
	{
		matr[i].clear();
	}
};

template <typename T>
std::pair <int, int> Matrix<T>::getSize()
{
	return{ row, col };
};

template <typename T>
Matrix <T> Matrix <T>::operator+ (const Matrix & matrix)
{
	if (col != matrix.col || row != matrix.row)
	{
		Matrix <T> Error(1, 1);
		std::cout << "different size";
		return Error;
	}
	Matrix <T> result(matrix.row, matrix.col);
	for (int i = 0; i < matrix.row; i++)
		for (int j = 0; j < matrix.col; j++)
			result[i][j] = (*this)[i][j] + matrix[i][j];
	return result;
};

template <typename T>
Matrix <T> Matrix <T>::operator+= (const Matrix & matrix)
{
	if (row != matrix.row || col != matrix.col)
	{
		Matrix <T> Error(1, 1);
		std::cout << "different size";
		return Error;
	}
	for (int i = 0; i < matrix.row; i++)
		for (int j = 0; j < matrix.col; j++)
			(*this)[i][j] = (*this)[i][j] + matrix[i][j];
	return (*this);
};

template <typename T>
Matrix <T> Matrix <T>::operator- (const Matrix &matrix)
{
	if (row != matrix.row || col != matrix.col)
	{
		Matrix <T> Error(1, 1);
		std::cout << "different size";
		return Error;
	}
	Matrix <T> result(matrix.row, matrix.col);
	for (int i = 0; i < matrix.row; i++)
		for (int j = 0; j < matrix.col; j++)
			result[i][j] = (*this)[i][j] - matrix[i][j];
	return result;
};

template <typename T>
Matrix <T> Matrix <T>::operator-= (const Matrix & matrix)
{
	if (row != matrix.row || col != matrix.col)
	{
		Matrix <T> Error(1, 1);
		std::cout << "different size";
		return Error;
	}
	for (int i = 0; i < matrix.row; i++)
		for (int j = 0; j < matrix.col; j++)
			(*this)[i][j] = (*this)[i][j] - matrix[i][j];
	return (*this);
};


template <typename T>
Matrix <T> Matrix <T>::operator* (const Matrix & matrix)
{
	if (col != matrix.row)
	{
		Matrix <T> Error(1, 1);
		std::cout << "wrong sizes";
		return Error;
	}
	else
	{
		Matrix <T> result(row, matrix.col);
		for (int i = 0; i < row; i++)
			for (int j = 0; j < matrix.col; j++)
				for (int k = 0; k < col; k++)
					result[i][j] += (*this)[i][k] * matrix[k][j];
		return result;
	}
};

template <typename T>
Matrix <T> Matrix <T>::operator* (const T & x)
{
	Matrix <T> result(row, col);
	for (int i = 0; i < row; i++)
		for (int j = 0; j < col; j++)
			result[i][j] = (*this)[i][j] * x;
	return result;
}


template <typename T>
Matrix <T> Matrix <T>::operator*= (const T & x)
{
	for (int i = 0; i < row; i++)
		for (int j = 0; j < col; j++)
			(*this)[i][j] = (*this)[i][j] * x;
	return (*this);
}


template <typename T>
Matrix <T> Matrix <T>::operator*= (const Matrix & matrix)
{
	if (col != matrix.row)
	{
		Matrix <T> Error(1, 1);
		std::cout << "wrong sizes";
		return Error;
	}
	else
	{
		Matrix <T> tmp(row, col);
		for (int i = 0; i < row; i++)
			for (int j = 0; j < matrix.col; j++)
				for (int k = 0; k < col; k++)
					tmp[i][j] += (*this)[i][k] * matrix[k][j];
		matr = tmp.matr;
		return (*this);
	}
}

template <typename T>
Matrix <T> Matrix <T>::operator/ (Matrix & matrix)
{
	Matrix <T> result(row, col);
	result = (*this) * matrix.inverse();
	return result;
}

template <typename T>
Matrix <T> Matrix <T>::operator/= (Matrix & matrix)
{
	(*this) *= matrix.inverse();
	return(*this);
}


template <typename T>
T Matrix <T>::track()
{
	T track = 0;
	int min = (row < col) ? row : col;
	for (int i = 0; i < min; i++)
		track += (*this)[i][i];
	return track;
}


template <typename T>
Matrix <T> Matrix <T>::transp()
{
	Matrix <T> transp(col, row);
	for (int i = 0; i < col; i++)
		for (int j = 0; j < row; j++)
			transp[i][j] = (*this)[j][i];
	return transp;
}


template <typename T>
int Matrix<T>::permutation(Matrix & matrix, const int & i)
{
	double tmp;
	int j = i + 1;
	while ((j < row) && (matrix[j][i] == 0)) j++;
	if (j < row)
	{
		for (int k = 0; k < row; k++)
		{
			tmp = matrix[i][k];
			matrix[i][k] = matrix[j][k];
			matrix[j][k] = tmp;
		}
	}
	else return -1;
	return j;
}


template <typename T>
long double Matrix<T>::determinant()
{
	if (row != col)
	{
		double error = 0;
		std::cout << "matrix is not squared ";
		return error;
	}
	else
	{
		Matrix <long double> tmp(row, col);
		for (int i = 0; i < row; i++)
			for (int j = 0; j < row; j++)
				tmp[i][j] = (*this)[i][j];
		long double d;
		long double det = 1;
		for (int i = 0; i < row - 1; i++)
		{
			if (tmp[i][i] == 0)
			{
				permutation(tmp, i);

				if (tmp[i][i] == 0)
					return 0;
				else
					det *= -1;
			}
			for (int j = i + 1; j < row; j++)
			{
				if (tmp[j][i] != 0)
				{
					d = tmp[j][i] / tmp[i][i];
					for (int k = 0; k < row; k++)
					{
						tmp[j][k] = tmp[j][k] - tmp[i][k] * d;
					}
				}
			}
		}
		for (int i = 0; i < row; i++)
			det *= tmp[i][i];
		return det;
	}
}


template <typename T>
Matrix <long double> Matrix <T>::inverse()
{
	Matrix <T> Error(1, 1);
	if (row != col)
	{
		std::cout << "matrix is not squared";
		return Error;
	}
	double det = this->determinant();
	if (det == 0)
	{
		std::cout << "matrix is singualar";
		return Error;
	}
	Matrix <long double> inverse(row, col);
	for (int i = 0; i < row; i++)
	{
		inverse[i].assign(row, 0);
		inverse[i][i] = 1;
	}
	int numPermRow;
	std::vector <long double> tmp;
	double d;
	Matrix <long double> copy(row, col);
	for (int i = 0; i < row; i++)
		for (int j = 0; j < row; j++)
			copy[i][j] = (*this)[i][j];
	for (int i = 0; i < row; i++)
	{
		if (copy[i][i] == 0)
		{
			if (i != row - 1)
			{
				numPermRow = permutation(copy, i);
				if (copy[i][i] == 0)
					return Error;
				tmp = inverse[i];
				inverse[i] = inverse[numPermRow];
				inverse[numPermRow] = tmp;
			}
			else return Error;
		}
		for (int j = 0; j < row; j++)
		{
			if (j != i)
			{
				if (copy[j][i] != 0)
				{
					d = copy[j][i] / copy[i][i];
					for (int k = 0; k < row; k++)
					{
						copy[j][k] = copy[j][k] - copy[i][k] * d;
						inverse[j][k] = inverse[j][k] - inverse[i][k] * d;
					}
				}
			}
		}
	}
	for (int i = 0; i < row; i++)
		for (int j = 0; j < row; j++)
			inverse[i][j] /= copy[i][i];
	return inverse;
}

template <typename T>
Matrix <T> Matrix <T>::powMat(const int & degree)
{
	if (row != col)
	{
		Matrix <T> Error(1, 1);
		std::cout << "wrong sizes: ";
		return Error;
	}
	Matrix <T> Identity(row, col);
	for (int i = 0; i < row; i++)
	{
		Identity[i].assign(row, 0);
		Identity[i][i] = 1;
	}
	if (degree == 0)
	{
		return Identity;
	}
	Matrix <T> result(row, col);
	Matrix <T> X(row, col);
	X.matr = matr;
	result.matr = Identity.matr;
	int degree1 = degree;
	int count = 1;
	while (degree1 > 1)
	{
		degree1 /= 2;
		count++;
	}
	for (int i = 0; i < count; i++)
	{
		if (degree & (1 << i))
		{
			result *= X;
		}
		X *= X;
	}
	return result;
}


int main()
{
	Matrix<long double> A(4, 4);
	std::cout << "THE FIRST MATRIX A: " << std::endl;
	std::cout << "sizes of A: " << A.getSize().first << " " << A.getSize().second << std::endl;
	std::cin >> A;
	std::cout << A;
	std::cout << "the deteminant is: " << A.determinant() << std::endl;
	std::cout << "the inverse is: " << std::endl;
	std::cout << A.inverse() << std::endl;
	std::cout << "the transp is: " << std::endl;
	std::cout << A.transp() << std::endl;
	std::cout << "the track is: " << A.track() << std::endl;
	std::cout << "A^7 : " << std::endl;
	std::cout << A.powMat(7) << std::endl;
	Matrix<long double> B(4, 4);
	std::cout << "THE SECOND MATRIX B: " << std::endl;
	std::cout << "sizes of B: " << B.getSize().first << " " << B.getSize().second << std::endl;
	std::cin >> B;
	std::cout << B;
	std::cout << "A + B =" << std::endl;
	std::cout << A + B;
	std::cout << "A * B + " << std::endl;
	std::cout << A * B;
	A *= 5;
	std::cout << "A * 5 = " << std::endl;
	std::cout << A;
	std::cout << "A / B = " << std::endl;
	std::cout << A / B;
	B /= A;
	std::cout << "B / A = " << std::endl;
	std::cout << B;
	std::cout << "B^13 = " << std::endl;
	std::cout << B.powMat(13);
	std::cout << "inverse B: " << std::endl;
	std::cout << B.inverse();
	Matrix <long double> C(4, 2);
	std::cout << "THE THIRD MATRIX C: " << std::endl;
	std::cout << "sizes of C: " << C.getSize().first << " " << C.getSize().second << std::endl;
	std::cin >> C;
	std::cout << C;
	std::cout << "the deteminant is: " << C.determinant() << std::endl;
	std::cout << "the inverse is: " << std::endl;
	std::cout << C.inverse() << std::endl;
	std::cout << "the transp is: " << std::endl;
	std::cout << C.transp() << std::endl;
	std::cout << "the track is: " << C.track() << std::endl;
	std::cout << "C^10 : " << std::endl;
	std::cout << C.powMat(10) << std::endl;
	std::cout << "A + C = " << std::endl;
	std::cout << A + C;
	std::cout << "A * C = " << std::endl;
	std::cout << A * C;
	return 0;
}
