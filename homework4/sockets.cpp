#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <stdio.h>
#include <cstdlib>
#include <queue>

using namespace std;

void buildUsed(vector < bool> & used, int N)
{
	for (int i = 0; i <= N; i++)
	{
		used.push_back(false);
	}
}

void buildDistanceFrom(vector <int> & distance, vector <int> & from, int N)
{
	for (int i = 0; i <= N; i++)
	{
		distance.push_back(0);
		from.push_back(0);
	}
}

void getSockets(map <int, vector <int> > & tasks, int N)
{
	vector <string> sockets;
	string buff;
	getline(cin, buff);
	for (int i = 0; i < N; i++)
	{
		string socket;
		getline(cin, socket);
		sockets.push_back(socket);
	}
	vector <vector <string> > connect;
	for (int j = 1; j <= N; j++)
	{
		size_t i;
		if (sockets[j - 1] == "")
		{
			tasks[j].push_back(-1);
		}
		else {
			while ((i = sockets[j - 1].find_first_of(" ")) != string::npos)
			{
				string p = sockets[j - 1].substr(0, i);
				tasks[j].push_back(atoi(p.c_str()));
				sockets[j - 1] = sockets[j - 1].substr(i + 1);
			}
			tasks[j].push_back(atoi(sockets[j - 1].c_str()));
		}
	}
}

void bfs(map <int, vector <int> > sockets, vector <bool>  & used, vector <int> & from, vector <int> & distance, int start, int finish)
{
	used[start] = true;
	distance[start] = 0;
	queue <int> q;
	q.push(start);
	while (!q.empty())
	{
		int v = q.front();
		q.pop();
		if (v == finish)
			break;
		for (int i = 0; i < sockets[v].size(); i++)
			if ((sockets[v][i] != -1) && (!used[sockets[v][i]]))
			{
				used[sockets[v][i]] = true;
				from[sockets[v][i]] = v;
				distance[sockets[v][i]] = distance[v] + 1;
				q.push(sockets[v][i]);
			}
	}
}

void print(const vector <int> & from, int start, int finish)
{
	if (from[finish] != start)
	{
		print(from, start, from[finish]);
	}
	cout << from[finish] << " " << finish << endl;
}

void printSolutions(int start, int finish, const vector <int> & distance, const vector <int> & from)
{
	cout << "_____ANSWER_____" << endl;
	cout << distance[finish] << endl;
	print(from, start, finish);
}

int main()
{
	int N;
	cin >> N;
	int start, finish;
	cin >> start >> finish;
	map <int, vector <int> > sockets;
	getSockets(sockets, N);
	vector <int> distance;
	vector <bool> used;
	vector <int> from;
	buildUsed(used, N);
	buildDistanceFrom(distance, from, N);
	bfs(sockets, used, from, distance, start, finish);
	if (!used[finish])
		cout << "No solutions" << endl;
	else
		printSolutions(start, finish, distance, from);
	return 0;
}
