#include<iostream>
#include<string>
#include<vector>
#include<map>
#include<set>

using namespace std;

class student
{
public:
	student(const string& first, const string& second)
	{
		name = first;
		surname = second;
	}
	void setMarks(string s, int mark)
	{
		marks[s] = mark;
	};
	int getMark(string s)
	{
		if (marks.find(s) == marks.end())
			return 0;
		else
			return marks.find(s)->second;
	};
	int countOfPositiveExam()
	{
		int flag = 0;
		for (auto it = marks.begin(); it != marks.end(); it++) // for (auto it: m)
		{
			if ((it->second) > 2)
			{
				flag++;
			}
		}
		return flag;
	};
	bool operator<(const student& st) const
	{
		return (name < st.name);
	};
	string getName() {
		return name;
	};
	string getSurname() {
		return surname;
	};
private:
	string name;
	string surname;
	map <string, int> marks;
};


class group
{
public:
	void addStudent(student* st)
	{
		students.insert(st);
	};
	void deduction(int count)
	{
		set <student*> ::iterator it = students.begin();
		do
		{
			if ((*it)->countOfPositiveExam() < count)
			{
				cout << (*it)->getName() << " " << (*it)->getSurname() << " is deduct" << endl;
				auto next = it++;
				students.erase(next);
			}
			else it++;
		} while (it != students.end());
	}
private:
	set <student*> students;
};

int main()
{
	student* student1 = new student("Ran", "Reynold");
	student* student2 = new student("Dan", "Teynold");
	student* student3 = new student("Ban", "Meynold");
	student* student4 = new student("Stan", "Keynold");
	student1->setMarks("math", 2);
	student1->setMarks("rus", 2);
	student1->setMarks("biology", 2);
	student2->setMarks("math", 10);
	student2->setMarks("chem", 3);
	student3->setMarks("math", 10);
	student3->setMarks("rus", 10);
	student3->setMarks("literarute", 3);
	student4->setMarks("math", 2);
	student4->setMarks("rus", 8);
	student4->setMarks("english", 2);
	group* Group1 = new group();
	Group1->addStudent(student1);
	Group1->addStudent(student2);
	Group1->addStudent(student3);
	Group1->addStudent(student4);
	Group1->deduction(2);
	delete student1;
	delete student2;
	delete student3;
	delete student4;
	delete Group1;
	return 0;
}
