#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <stdio.h>
#include <cstdlib>

using namespace std;

void getTasks(map <int, vector <int> > * tasks, int N)
{
	vector <string> linesOfTasks;
	string buf;
	getline(cin, buf);
	for (int i = 0; i < N; i++)
	{
		string oneLine;
		getline(cin, oneLine);
		linesOfTasks.push_back(oneLine);
	}
	vector <vector <string> > connect;
	for (int j = 1; j <= N; j++)
	{
		size_t i;
		if (linesOfTasks[j - 1] == "")
		{
			(*tasks)[j].push_back(-1);
		}
		else
		{
			while ((i = linesOfTasks[j - 1].find_first_of(" ")) != string::npos)
			{
				string p = linesOfTasks[j - 1].substr(0, i);
				(*tasks)[j].push_back(atoi(p.c_str()));
				linesOfTasks[j - 1] = linesOfTasks[j - 1].substr(i + 1);
			}
			(*tasks)[j].push_back(atoi(linesOfTasks[j - 1].c_str()));
		}
	}
}


vector <int> getFirstSolutions(vector <int> solutions, map <int, vector <int> > tasks)
{
	for (auto it : tasks)
	{
		if (it.second[0] == -1)
		{
			solutions.push_back(it.first);
		}
	}
	return solutions;
}

void reduceTasks(map <int, vector <int> > * tasks, vector <int> solutions)
{
	for (int i = 0; i < solutions.size(); i++)
	{
		(*tasks).erase(solutions[i]);
	}
}

vector <int> completeTasks(map <int, vector <int> > tasks, vector <int> solutions)
{
    solutions = getFirstSolutions(solutions, tasks);
	do
	{
		int flag = tasks.size();
		for (auto it : tasks)
		{
			if (it.second[0] == 0)
			{
				solutions.clear();
				solutions.push_back(0);
				return solutions;
			}
			for (int k = 0; k < it.second.size(); k++)
			for (int j = 0; j < solutions.size(); j++)
			{
				if (it.second[k] == solutions[j])
				{
					it.second.erase(it.second.begin() + k);
					if (k != 0)
					{
						k--;
					}
					j = -1;
				}
				if (it.second.empty())
				{
					solutions.push_back(it.first);
					j = solutions.size();
				}
			}
		}
		reduceTasks(&tasks, solutions);
		if (flag == tasks.size())
		{
			solutions.clear();
			solutions.push_back(0);
			return solutions;
		}
	} while (tasks.size() != 0);
	return solutions;
}

void printSolutions(vector <int> solutions)
{
	for (int i = 0; i < solutions.size(); i++)
	{
		cout << solutions[i] << endl;
	}
}

int main()
{
	int N;
	cin >> N;
	map <int, vector <int> > tasks;
	getTasks(&tasks, N);
	vector <int> solutions;
	solutions = completeTasks(tasks, solutions);
	printSolutions(solutions);
}
