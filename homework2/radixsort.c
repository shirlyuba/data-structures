#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>


void radix (int* a, int n)
{
	int i,j, max;
	max = a[0];
	for(i = 1; i < n; ++i)
	{
		if (max < a[i])
		{
			max = a[i];
		}
	}
	int digit = 0;
	while(max != 0)
	{
		max /= 10;
		++digit;
	}
	int** buf = (int**)malloc(10 * sizeof(int*));
	for (i = 0;i < 10;++i)
	{
		buf[i] = (int*)malloc(sizeof(int)*n);
	}
	for (i = 0; i < 10; ++i)
	{
		buf[i][0] = 0;
	}
	int b;
	for (i = 0; i < digit; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			b = (a[j] % (int)exp(log(10.0) * (i + 1)));
			b /= (int)exp(log(10.0) * (i));
			++buf[b][0];
			buf[b][buf[b][0]] = a[j];
		}
		int k,t = 0;
		for (j = 0; j < 10; ++j)
		{
			k = 0;
			while(buf[j][0] != k)
			{
				a[t++] = buf[j][k+1];
				buf[j][k+1] = 0;
				++k;
			}
			buf[j][0] = 0;
		}
	}
	for (i = 0; i < 10; ++i)
	{
		free(buf[i]);
	}
	free(buf);
}


void GetMassInt(int n, int *a)
{
	int i;
	srand(time(NULL));
	for (i = 0; i < n; i++)
	{
		a[i] = rand();
	}
}


int check(int* a, int n)
{
	int i;
	for (i = 0; i < n - 1; i++)
	{
		if (a[i] > a[i+1])
		{
			return 0;
		}
	}
    return 1;
}


int main()
{
	printf("Enter the count of elements: ");
	int n;
	scanf("%d", &n);
	int* a = (int*)malloc(n * sizeof(int));
	GetMassInt(n, a);
	radix(a, n);
	if (check(a, n))
	{
		printf("true");
	}
	else
	{
		printf("false");
	}
	printf("\n");
	free(a);
	return 0;
}
