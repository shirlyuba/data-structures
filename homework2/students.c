#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>


typedef struct
{
	int IDnumber;
	char surname[29];
	char name[5];
	int mark;
} students;


int compare(const void* i,const void* j);
students getStudent();
void printStudent(students a);
void printStudentsList(students* a, int n);


int main()
{
	int n;
	printf("Enter the count of students\n");
	scanf("%d", &n);
	students* mas = (students *)malloc(n * sizeof(students));
	int i;
	printf("Enter data of students\n");
	for( i= 0; i < n; i++)
	{
        mas[i] = getStudent();
	}
	int l = 0;
	int r = n - 1;
	qsort(mas , n, sizeof(students), compare);
	printStudentsList(mas, n);
	free(mas);
	return 0;
}


int compare(const void* a, const void* b)
{
	students x = *((students*)a);
	students y = *((students*)b);
	return -(x.mark - y.mark);
}


students getStudent()
{
	students data;
	scanf("%d %s %s %d", &data.IDnumber, data.surname, data.name, &data.mark);
	return(data);
}


void printStudent(students a)
{
	printf("%d %s %s %d\n", a.IDnumber, a.surname, a.name, a.mark);
}


void printStudentsList(students* a, int n)
{
	int i;
	for ( i = 0; i < n; i++)
	{
		printStudent(a[i]);
	}
}
