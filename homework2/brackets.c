#include<stdio.h>
#include<malloc.h>
#include<string.h>

typedef struct Node
{
	struct Node* next;
	struct Node* prev;
	char value;
}Node;

typedef struct Stack
{
	struct Node* first;
	struct Node* last;
}Stack;


Stack* stack_new();
void push(Stack* S, char* a);
char pop(Stack* S);


int main()
{
	printf("enter bracket impression:\n");
	char  ch;
	ch = getchar();
	Stack* S = stack_new();
	if ((ch == '(') || (ch == '{'))
	{
		for (; ch != '\n';)
		{
			if ((ch == '(') || (ch == '{'))
			{
				push(S, &ch);
			}
			else
			{
				if (S->last == NULL)
				{
					printf("NO");
					return 0;
				}
				else
				{
					if (((ch == ')') && (S->last->value == '(')) || ((ch == '}') && (S->last->value == '{')))
					{
						pop(S);
					}
					else
					{
						printf("NO");
						return 0;
					}
				}
			}
			ch = getchar();
		}
		if (S->last == NULL)
		{
			printf("YES");
		}
		else
		{
			printf("NO");
		}
	}
	else
	{
		printf("NO");
	}
	free(S);
	return 0;
}


Stack* stack_new()
{
	Stack* empty = (Stack*)malloc(sizeof(Stack));
	empty->last = empty->first = NULL;
	return (empty);
}


void push(Stack* l, char* a)
{
	Node* n = (Node*)malloc(sizeof(Node));
	n->value = *a;
	n->next = NULL;
	n->prev = l->last;
	if (l->last != NULL)
	{
		l->last->next = n;
	}
	l->last = n;
	if (l->first == NULL)
	{
		l->first = n;
	}
}


char pop(Stack* l)
{
	Node *next;
	char *tmp = (char*)malloc(sizeof(char));
	if (l->last == NULL)
	{
		return 0;
	}
	next = l->last;
	l->last = l->last->prev;
	if (l->last)
	{
		l->last->next = NULL;
	}
	if (next == l->first)
	{
		l->first = NULL;
	}
	*tmp = next->value;
	free(next);
	return *tmp;
}

