#include<stdio.h>
#include<string.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>


int CompareChar(void* a, void* b);
int CompareInt(void* a, void* b);
int CompareFloat(void* a, void* b);
int CompareDouble(void* a, void* b);
void* PositionInArray(void* a, int i, int sizeoftype);
void SwapVoid(void *base, int a, int b, int sizeoftype);
void GetMassInt(int n,int *a);
void GetMassFloat(int n,float *a);
void GetMassDouble(int n,double *a);
void PrintMassFloat( float* a, int n);
void PrintMassInt( int* a, int n);
void PrintMassChar( char* a, int n);
void PrintMassDouble( double* a, int n);
void GetMassChar(int n,char *a);
int check(void* a, int n, int sizeoftype, int(*compare)(void* a, void* b));
void AssignValue(void* a, int i, int j, int sizeoftype);
void insertionSort(void * a, int l, int r, int sizeoftype, int(*compare)(void*, void*));
void QuickSortMain(void* Array, int left, int right, int sizeoftype, int(*compare)(void*, void*));
void QuickSort(void* a, int n, int sizeoftype, int(*compare)(void*, void*));
void randomGeneration(int* arr,int count);;


int main()
{
	int type;
	printf("chose among types of data you want to sort: 1 - int, 2 - float, 3 - double, 4 - char: ");
	scanf("%d", &type);
	printf("enter the count of elements: ");
	int n;
	scanf("%d", &n);
	switch (type)
	{
	case 1:
		{
			int* a = (int*)malloc(n*sizeof(int));
			GetMassInt(n, a);
			QuickSort(a, n, sizeof(int), CompareInt);
			/*PrintMassInt(a, n);*/
			if (check(a, n, sizeof(int), CompareInt))
			{
				printf("true");
			}
			else
			{
				printf("false");
			}
			break;
		}
	case 2:
		{
			float* a = (float*)malloc(n * sizeof(float));
			GetMassFloat(n, a);
			QuickSort(a, n, sizeof(float), CompareFloat);
			/*PrintMassFloat(a, n);*/
			if (check(a, n, sizeof(float), CompareFloat))
			{
				printf("true");
			}
			else
			{
				printf("false");
			}
			break;
		}
	case 3:
		{
			double* a = (double*)malloc(n * sizeof(double));
			GetMassDouble(n, a);
			QuickSort(a, n, sizeof(double), CompareDouble);
			/*PrintMassDouble(a, n);*/
			if (check(a, n, sizeof(double), CompareDouble))
			{
				printf("true");
			}
			else
			{
				printf("false");
			}
			break;
		}
	case 4:
		{
			char* a = (char*)malloc(n * sizeof(char));
			GetMassChar(n, a);
			QuickSort(a, n, sizeof(char), CompareChar);
			/*PrintMassChar(a, n);*/
			if (check(a, n, sizeof(char), CompareChar))
			{
				printf("true");
			}
			else
			{
				printf("false");
			}
			break;
		}
	default: printf("your choice is wrong!");
	}
	printf("\n");
	return 0;
}


void QuickSortMain(void* a, int left, int right, int sizeoftype, int(*compare)(void*, void*))
{
	int r = right;
	int l = left;
	void* pivot = (void*)malloc(sizeoftype);
	memcpy(pivot, (void*)((char*)a + ((left + right)/2)*sizeoftype), sizeoftype);
	while(l < r)
	{
		while (compare(pivot, PositionInArray(a, l, sizeoftype)) > 0)
		{
			l++;
		}
		while (compare(PositionInArray(a, r, sizeoftype), pivot) > 0)
		{
			r--;
		}
		if (l <= r)
		{
			SwapVoid(a, l, r, sizeoftype);
			l++;
			r--;
		}
	}
	if (r > left)
	{
		QuickSortMain(a, left, r, sizeoftype, compare);
	}
	if (l < right)
	{
		QuickSortMain(a, l, right, sizeoftype, compare);
	}
}


void QuickSort(void* a, int n, int sizeoftype, int(*compare)(void*, void*))
{
	if (n<20)
	{
		insertionSort(a, 0, n-1, sizeoftype, compare);
	}
	else
	{
		QuickSortMain(a, 0, n - 1, sizeoftype, compare);
	}
}


void randomGeneration(int * arr, int count)
{
	srand(1000000);
	int i;
	for (i = 0; i < count; i++)
	{
		arr[i] = rand();
	}
}


void insertionSort(void * a, int l, int r, int sizeoftype, int(*compare)(void*, void*))
{
	int i;
	for (i = l + 1; i <= r; ++i)
	{
		int j = i - 1;
		void* v = (void*)malloc(sizeoftype);
		memcpy(v, (char*)a + i * sizeoftype, sizeoftype);
		while ((compare(PositionInArray(a, j, sizeoftype), v)>0) && ( j >= l ))
		{
			AssignValue(a, j+1, j, sizeoftype);
			--j;
		}
		memcpy((char*)a + (j + 1) * sizeoftype, v ,sizeoftype);
		free(v);
	}
}


void AssignValue(void* a, int i, int j, int sizeoftype)
{
	memcpy((char*)a+i*sizeoftype, (char*)a+j*sizeoftype, sizeoftype);
}


int CompareChar(void* a, void* b)
{
	if (*((char*)a) - *((char*)b) > 0)
	{
		return 1;
	}
	if (*((char*)a) - * (( char *)b) < 0)
	{
		return -1;
	}
	return 0;
}


int CompareInt(void* a, void* b)
{
	return *((int*)a) - *((int*)b);
}


int CompareFloat(void* a, void* b)
{
	if ((*((float*)a) - *((float*)b)) > 0)
	{
		return 1;
	}
	else
	{
		if ((*((float*)a) - *((float*)b)) < 0)
		{
			return -1;
		}
	}
	return 0;
}


int CompareDouble(void* a, void* b)
{
	if ((*((double*)a) - *((double*)b)) > 0)
	{
		return 1;
	}
	else
	{
		if ((*((double*)a) - *((double*)b)) < 0)
		{
			return -1;
		}
	}
	return 0;
}


void* PositionInArray(void* a, int i, int sizeoftype)
{
	return (char*)a + i*sizeoftype;
}


void SwapVoid(void *base, int a, int b, int sizeoftype)
{
	void* buf = malloc(sizeoftype);
	memcpy(buf, (char*)base + b * sizeoftype, sizeoftype);
	memcpy((char*)base + b * sizeoftype, (char*)base + a * sizeoftype, sizeoftype);
	memcpy((char*)base + a * sizeoftype, buf, sizeoftype);
	free(buf);
}


void GetMassInt(int n,int *a)
{
	int i;
	srand(time(NULL));
	for (i = 0; i < n; i++)
	{
		a[i] = rand();
	}
}


void GetMassFloat(int n,float *a)
{
	srand(time(NULL));
	int i;
	for (i = 0; i < n; i++)
	{
		a[i] = rand() / 123.456;
	}
}


void GetMassDouble(int n, double *a)
{
	srand(time(NULL));
	int i;
	for (i = 0; i < n; i++)
	{
		a[i] = rand() / 9876.54321;
	}
}


void PrintMassFloat( float* a, int n)
{
	int i;
	for(i = 0; i < n; i++)
	{
		printf("%g ", a[i]);
	}
}


void PrintMassInt( int* a, int n)
{
	int i;
	for(i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
}


void PrintMassChar( char* a, int n)
{
	int i;
	for(i = 0; i < n; i++)
	{
		printf("%c ", a[i]);
	}
}


void PrintMassDouble( double* a, int n)
{
	int i;
	for(i = 0; i < n; i++)
	{
		printf("%lf ", a[i]);
	}
}


void GetMassChar(int n,char *a)
{
	srand(time(NULL));
	int i;
	for (i = 0; i < n; i++)
	{
		a[i] = (char)(rand() % 255);
	}
}

int check(void* a, int n, int sizeoftype, int(*compare)(void* a, void* b))
{
	int i;
	for (i = 0; i < n - 1; i++)
	{
		if (compare(PositionInArray(a, i, sizeoftype), PositionInArray(a, i + 1, sizeoftype)) > 0)
		{
			return 0;
		}
	}
	return 1;
}
