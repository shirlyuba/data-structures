#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
#include<string.h>

struct Pair
{
	double  key;
	char info[33];
};

struct Bheap
{
	struct Pair* queue;
	int size;
	int data_size;
};


struct Bheap* New(int size ) ;
void Delete(struct Bheap* h) ;
void Checkup(struct Bheap* h, int c ) ;
void Checkdown(struct Bheap* h, int p ) ;
void Add(struct Bheap* h, struct Pair v ) ;
int ExtractMin(struct Bheap* h, struct Pair* v);


int main()
{
	struct Bheap* h = New(1000);
	struct Pair v = {0, "\0\0"};
	char s[10];
	char str1[] = "FINISH";
	char str2[] = "ADD";
	char str3[] = "PROCESS";
	do
	{
		printf("Write command:\n\n");
		scanf("%s",s);
			if (strcmp(s,str2) == 0)
			{
				scanf("%lf %s",&v.key,&v.info);
				Add(h,v);
				printf("The highest priority at the moment - %g.\n\n",h->queue[1].key);
			}
			if (strcmp(s,str3) == 0)
			{
					if (ExtractMin(h,&v))
					{
						printf("Task '%s' is done.\n\n",v.info);
					}
					else
					{
						printf("\nAll tasks are done.\n");
					}
			}
			if (strcmp(s,str1) == 0)
			{
				while(ExtractMin(h, &v))
				{
					printf("Task '%s' is done.\n\n",v.info);
				}
				printf("All tasks are done.\n");
			}
	} while(strcmp(s,str1) != 0);
	Delete( h );
	return 0;
}


struct Bheap* New(int size )
{
	struct Bheap* h = (struct Bheap*) malloc( sizeof(struct Bheap) );
	h->queue = (struct Pair*) malloc( sizeof(struct Pair) * (1 + size) );
	h->data_size = 1 + size;
	h->size = 0;
	return h;
}


void Delete(struct Bheap* h)
{
	if( h )
	{
		if( h->queue )
		{
			free( h->queue );
		}
		free(h);
	}
}


void Checkup(struct Bheap* h, int c )
{
	int p;
	for( p = c / 2;  p > 0 ; c = p , p = c / 2 )
	{
		if( h->queue[p].key > h->queue[c].key )
		{
			struct  Pair tmp = h->queue[p];
			h->queue[p] = h->queue[c];
			h->queue[c] = tmp;
		}
		else
		{
			break;
		}
	}
}


void Checkdown(struct Bheap* h, int p )
{
	int c;
	for( c = 2 * p ;  c <= h->size  ; p = c, c = 2 *p )
	{
		if( (c + 1 <= h->size) && (h->queue[c + 1].key < h->queue[c].key) )
		{
			c++;
		}
		if( h->queue[c].key < h->queue[p].key )
		{
			struct Pair  tmp;
			tmp =  h->queue[c];
			h->queue[c] = h->queue[p];
			h->queue[p] = tmp;
		}
		else
		{
			break;
		}
	}
}


void Add(struct Bheap* h,struct Pair v )
{
	if( h->size + 1 >= h->data_size )
	{
		h->data_size *= 2;
		h->queue = (struct Pair*) realloc( h->queue, h->data_size * sizeof(struct Pair) );
	}
	h->size++;
	h->queue[h->size] = v ;
	Checkup( h, h->size );
}


int ExtractMin(struct Bheap* h, struct Pair* v)
{
	if( h->size == 0 )
	{
		return 0;
	}
	*v = h->queue[1];
	h->queue[1] = h->queue[ h->size];
	h->size--;
	Checkdown( h, 1 );
	return 1;
}
