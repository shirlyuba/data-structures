#include<stdio.h>
#include<malloc.h>

struct Pair
{
	int  key;
	int value;
};

struct Bheap
{
	struct Pair *data;
	int size;
	int data_size;
};

void BuildBheap(int n, struct Bheap* h, struct Pair v);
struct Bheap* BheapNew(int size ) ;
void BheapDelete(struct Bheap* h) ;
void BheapCheckup(struct Bheap* h, int c ) ;
void BheapCheckdown(struct Bheap* h, int p ) ;
void BheapAdd(struct Bheap* h, struct Pair v ) ;
int BheapExtractMin(struct Bheap* h,struct Pair* v);
void HeapSort(struct Bheap* h, int* a);


int main()
{
	struct Bheap* h = BheapNew(1000);
	int n, i;
	struct Pair v = {0, 0};
	printf("Enter count of elements\n");
	scanf("%d", &n);
	int* a = (int*)malloc(sizeof(int) * n);
	printf("Enter value of elements:\n");
	BuildBheap(n, h, v);
	printf("Your heap:\n");
    for (i = 1; i < n+1; i++)
	{
		printf("%d ",h->data[i].key);
	}
	printf("\n");
	HeapSort(h, a);
	printf("Sorted heap:\n");
	for (i = 0;i < n;i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
	free(a);
	BheapDelete( h );
	return 0;
}
struct Bheap* BheapNew(int size )
{
	struct Bheap* h = (struct Bheap*) malloc( sizeof(struct Bheap) );
	h->data = (struct Pair*) malloc( sizeof(struct Pair) * (1 + size) );
	h->data_size = 1 + size;
	h->size = 0;
	return h;
}

void BheapDelete(struct Bheap* h)
{
	if( h )
	{
		if( h->data )
		{
			free( h->data );
		}
		free(h);
	}
}


void BuildBheap(int n, struct Bheap* h, struct Pair v)
{
	int i;
	for( i = 0; i < n; i++ )
	{
		scanf("%d", &v.key);
		v.value = i;
		BheapAdd(h, v);
	}
}


void BheapCheckup(struct Bheap* h, int c )
{
	int p;
	for( p = c / 2;  p > 0 ; c = p , p = c / 2 )
	{
		if( h->data[p].key > h->data[c].key )
		{
			struct Pair tmp = h->data[p];
			h->data[p] = h->data[c];
			h->data[c] = tmp;
		}
		else
		{
			break;
		}
	}
}

void BheapCheckdown(struct Bheap* h, int p )
{
	int c;
	for( c = 2 * p ;  c <= h->size  ; p = c, c = 2 *p )
	{
		if( (c + 1 <= h->size) && (h->data[c + 1].key < h->data[c].key) )
		{
			c++;
		}
		if( h->data[c].key < h->data[p].key )
		{
			struct Pair  tmp;
			tmp =  h->data[c];
			h->data[c] = h->data[p];
			h->data[p] = tmp;
		}
		else
		{
			break;
		}
	}
}


void BheapAdd(struct Bheap* h, struct Pair v )
{
	if( h->size + 1 >= h->data_size )
	{
		h->data_size *= 2;
		h->data = (struct Pair* ) realloc( h->data, h->data_size * sizeof(struct Pair) );
	}
	h->size++;
	h->data[h->size] = v ;
	BheapCheckup( h, h->size );
}


int BheapExtractMin(struct Bheap* h, struct Pair* v)
{
	if( h->size == 0 )
	{
		return 0;
	}
	*v = h->data[1];
	h->data[1] = h->data[ h->size];
	h->size--;
	BheapCheckdown( h, 1 );
	return 1;
}


void HeapSort(struct Bheap* h, int* a){
	int i = 0;
	struct Pair t = {0,0};
	while ( BheapExtractMin(h,&t) )
	{
		a[i] = t.key;
		i++;
	}
}
