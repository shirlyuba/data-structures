#include<stdlib.h>
#include<stdio.h>
#include<string.h>

void swap(int* a, int* b)
{
	int c = *a;
	*a = *b;
	*b = c;
}


int partition(int* a, int l, int r)
{
	int pivot = rand();
	swap(&pivot, &a[r]);
	int i = l - 1;
	int j = r;
	while (i < j)
	{
		while ((a[++i] < pivot) && (i < r));
		while ((j > i) && (a[--j] >= pivot));
		if (j <= i)
		{
			break;
		}
		swap(&a[i], &a[j]);
	}
	swap(&pivot, &a[r]);
	swap(&a[i], &a[r]);
	return i;
}


int OrderStatistic(int* a,int l, int r, int k)
{
	if (r == l)
	{
		return a[r];
	}
	int mid = partition(a, l, r);
	if (k == mid - l)
	{
		return a[mid];
	}
	if (mid - l > k)
	{
		return OrderStatistic(a, l, mid - 1, k);
	}
	else
	{
		return OrderStatistic(a, mid + 1, r, k - mid - 1);
	}
}

void PrintfMatrix(int** Matrix, int n, int m)
{
	int i, j;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			printf("%d", Matrix[i][j]);
			if (Matrix[i][j] < 10 && n != 1)
			{
				printf("   ");
			}
			if (Matrix[i][j] >= 10 && Matrix[i][j] < 100 && n != 1)
			{
				printf("  ");
			}
			if (Matrix[i][j] >= 100 && n != 1)
			{
				printf(" ");
			}
			if (n == 1 && j != m - 1)
			{
				printf(" ");
			}
		}
		if (n != 1)
		{
			printf("\n");
		}
	}
	if (n != 1)
	{
		printf("\n");
	}
}


int** CreateMatrix(int a, int b)
{
	int** Matrix = (int**)malloc(sizeof(int*) * a);
	int i;
	for (i = 0; i < a; i++)
	{
		Matrix[i] = (int*)malloc(sizeof(int) * b);
	}
	return Matrix;
}


void WriteMatrix(int** Matrix, int a, int b)
{
	int i, j;
	for (i = 0; i < a; i++)
	{
		for (j = 0; j < b; j++)
		{
			scanf("%d", &Matrix[i][j]);
		}
	}
}


int main()
{
	int m, n;
	scanf("%d %d", &m, &n);
	int** Matrix = CreateMatrix(m, n);
	WriteMatrix(Matrix, m, n);
	int* VecB = (int*)malloc(sizeof(int) * m);
	int i;
	for (i = 0; i < m; i ++)
	{
		scanf("%d", &VecB[i]);
	}
	printf("\n");
	PrintfMatrix(Matrix, m, n);
	printf("Vector B: (");
	for (i = 0; i < m; i++)
	{
		printf("%d ", VecB[i]);
	}
	printf(")\n");
	printf("Vector C: (");
	int* VecC = (int*)malloc(sizeof(int) * m);
	for ( i =0; i < m; i++)
	{
		VecC[i] = OrderStatistic(Matrix[i], 0, m-1, VecB[i]);
	}
	for (i = 0; i < m; i++)
	{
		printf("%d ", VecC[i]);
	}
	printf(")\n");
	free(Matrix);
	free(VecB);
	free(VecC);
	return 0;
}
