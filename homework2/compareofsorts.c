#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>


/* User should change type of data, he want to use,( in comparator, signature of function, etc) ShellSort and RadixSort are using int */



struct Pair
{
	int  key;
	int value;
};

struct Bheap
{
	struct Pair *data;
	int size;
	int data_size;
};


void ShellSort(int* a, int n);
void HeapSort(struct Bheap* h, int* a);
void BuildBheap(int* a, int n, struct Bheap* h, struct Pair v);
void BheapCheckup(struct Bheap* h, int c );
struct Bheap* BheapNew(int size );
void BheapCheckdown(struct Bheap* h, int p );
void BheapAdd(struct Bheap* h, struct Pair v );
int BheapExtractMin(struct Bheap* h, struct Pair* v);
void AssignValue2(void* a, void* b, int i, int j, int sizeoftype);
void merge(void* a, int l, int m, int r, int sizeoftype, int(*compare)(const void* a, const void*b), int* buf);
void MergeSort(void* a, int l, int r, int sizeoftype, int(*compare)(const void* a,const  void* b), int* buf);
void radix (int* a, int n);
void* PositionInArray(void* a, int i, int sizeoftype);
void SwapVoid(void *base, int a, int b, int sizeoftype);
void QuickSort(void* a, int left, int right, int sizeoftype, int(*compare)(const void* a, const void* b));
void insertionSort(void * a, int l, int r, int sizeoftype, int(*compare)(const void* a, const void* b));
void ShellSort(int* a, int n);
void AssignValue(void* a, int i, int j, int sizeoftype);
void reload(int* arr1, int* arr2,int n);
void QuickSortOpt(void* a, int n, int sizeoftype, int(*compare)(const void* a, const void* b));
void check(int* arr, int n, char* s, double time);
int comparator(const void* a, const void* b);


int main()
{
	srand(time(NULL));
	int i,n;
	printf("Enter the number of elements\n");
	scanf("%d",&n);
	int* mas = (int*)malloc(sizeof(int)*n);
	int* mas2 = (int*)malloc(sizeof(int)*n);
	for (i = 0; i < n; ++i)
	{
		mas[i] = rand();
		mas2[i] = mas[i];
	}
	clock_t begin, end;
	double timesort;
	begin = clock();
	radix(mas2,n);
	end = clock();
	timesort = (double)(end - begin)/CLOCKS_PER_SEC;
	check(mas2,n,"Radix",timesort);
	reload(mas, mas2, n);
	struct Bheap *h = BheapNew(1000);
	struct Pair v = {0, 0};
	BuildBheap(mas2, n, h, v);
	begin = clock();
	HeapSort(h, mas2);
	end = clock();
	timesort = (double)(end - begin)/CLOCKS_PER_SEC;
	check(mas2, n, "HeapSort", timesort);
	reload(mas, mas2, n);
	begin = clock();
	QuickSortOpt(mas2, n, sizeof(int), comparator);
	end = clock();
	timesort = (double)(end - begin)/CLOCKS_PER_SEC;
	check(mas2, n, "QuickSortOpt", timesort);
	reload(mas, mas2, n);
	int* buf = (int*)malloc(sizeof(int)*(n+1));
	begin = clock();
	MergeSort(mas2, 0, n - 1, sizeof(int), comparator, buf);
	end = clock();
	timesort = (double)(end - begin)/CLOCKS_PER_SEC;
	check(mas2, n, "MergeSort",timesort);
	reload(mas, mas2, n);
	begin = clock();
	qsort(mas2, n, sizeof(int), comparator);
	end = clock();
	timesort = (double)(end - begin)/CLOCKS_PER_SEC;
	check(mas2, n, "StandartQsort",timesort);
	reload(mas, mas2, n);
	begin = clock();
	QuickSort(mas2, 0, n-1, sizeof(int), comparator);
	end = clock();
	timesort = (double)(end - begin)/CLOCKS_PER_SEC;
	check(mas2, n, "QuickSort", timesort);
	reload(mas, mas2, n);
	begin = clock();
	ShellSort(mas2, n);
	end = clock();
	timesort = (double)(end - begin)/CLOCKS_PER_SEC;
	check(mas2, n, "ShellSort", timesort);
	free(mas);
	free(mas2);
	return 0;
}


int comparator(const void* a, const void* b)
{
	const int* x = (const int*) a;
	const int* y = (const int*) b;
	if (*x > *y)
	{
		return 1;
	}
	if (*x < *y)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}


void HeapSort(struct Bheap* h, int* a)
{
	int i = 0;
	struct Pair t = {0,0};
	while ( BheapExtractMin(h,&t) )
	{
		a[i] = t.key;
		i++;
	}
}


void BuildBheap(int* a, int n, struct Bheap* h, struct Pair v)
{
	int i;
	for( i = 0; i < n; i++ )
	{
		v.key = a[i];
		v.value = i;
		BheapAdd(h, v);
	}
}


struct Bheap* BheapNew(int size )
{
	struct Bheap* h = (struct Bheap*) malloc( sizeof(struct Bheap) );
	h->data = (struct Pair*) malloc( sizeof(struct Pair) * (1 + size) );
	h->data_size = 1 + size;
	h->size = 0;
	return h;
}

void BheapCheckdown(struct Bheap* h, int p )
{
	int c;
	for( c = 2 * p ;  c <= h->size  ; p = c, c = 2 *p )
	{
		if( (c + 1 <= h->size) && (h->data[c + 1].key < h->data[c].key) )
		{
			c++;
		}
		if( h->data[c].key < h->data[p].key )
		{
			struct Pair  tmp;
			tmp =  h->data[c];
			h->data[c] = h->data[p];
			h->data[p] = tmp;
		}
		else
		{
			break;
		}
	}
}

void BheapAdd(struct Bheap* h, struct Pair v )
{
	if( h->size + 1 >= h->data_size )
	{
		h->data_size *= 2;
		h->data = (struct Pair* ) realloc( h->data, h->data_size * sizeof(struct Pair) );
	}
	h->size++;
	h->data[h->size] = v ;
	BheapCheckup( h, h->size );
}


int BheapExtractMin(struct Bheap* h, struct Pair* v)
{
	if( h->size == 0 )
	{
		return 0;
	}
	*v = h->data[1];
	h->data[1] = h->data[ h->size];
	h->size--;
	BheapCheckdown( h, 1 );
	return 1;
}


void AssignValue2(void* a, void* b, int i, int j, int sizeoftype)
{
	memcpy((void*)((char*)a + i * sizeoftype), (void*)((char*)b + j * sizeoftype), sizeoftype);
}


void merge(void* a, int l, int m, int r, int sizeoftype, int(*compare)(const void* a,const void* b), int* buf)
{
	int i, j, k;
	for ( i = m + 1; i > l; --i)
		AssignValue2(buf, a, i - 1, i - 1, sizeoftype);
	for (j = m; j < r; ++j)
		AssignValue2(buf, a, r + m - j, j + 1, sizeoftype);
	for ( k = l; k <= r; ++k)
	{
		if (compare(PositionInArray(buf, i, sizeoftype), PositionInArray(buf, j, sizeoftype)) > 0)
		{
			AssignValue2(a, buf, k, j, sizeoftype);
			--j;
		}
		else
		{
			AssignValue2(a, buf, k, i, sizeoftype);
			++i;
		}
	}
	for (i = 0; i <= r - l; ++i)
	{
		buf[i] = 0;
	}
}


void MergeSort(void* a, int l, int r, int sizeoftype, int(*compare)(const void* a, const void* b), int* buf)
{
	if (l >= r)
	{
		return;
	}
	int m = (l+r)/ 2;
	MergeSort(a, l, m, sizeoftype, compare, buf);
	MergeSort(a, m+1, r, sizeoftype, compare, buf);
	merge(a, l, m, r, sizeoftype, compare, buf);
}

void radix (int* a, int n)
{
	int i,j, max;
	max = a[0];
	for(i = 1; i < n; ++i)
	{
		if (max < a[i])
		{
			max = a[i];
		}
	}
	int digit = 0;
	while(max != 0)
	{
			max /= 10;
			++digit;
	}
	int** buf = (int**)malloc(10*sizeof(int*));
	for (i = 0;i < 10;++i)
	{
		buf[i] = (int*)malloc(sizeof(int)*n);
	}
	for (i = 0; i < 10; ++i)
	{
		buf[i][0] = 0;
	}
	int b;
	for (i = 0; i < digit; ++i)
	{
		for (j = 0; j < n; ++j)
		{
			b = (a[j]%(int)exp(log(10.0)*(i + 1)));
			b /= (int)exp(log(10.0)*(i));
			++buf[b][0];
			buf[b][buf[b][0]] = a[j];
		}
		int k,t = 0;
		for (j = 0; j < 10; ++j)
		{
			k = 0;
			while(buf[j][0] != k)
			{
				a[t++] = buf[j][k+1];
				buf[j][k+1] = 0;
				++k;
			}
			buf[j][0] = 0;
		}
	}
	for (i = 0; i < 10; ++i)
		free(buf[i]);
	free(buf);
}


void* PositionInArray(void* a, int i, int sizeoftype)
{
	return (char*)a + i*sizeoftype;
}


void SwapVoid(void *base, int a, int b, int sizeoftype)
{
	void* buf = malloc(sizeoftype);
	memcpy(buf, (char*)base + b * sizeoftype, sizeoftype);
	memcpy((char*)base + b * sizeoftype, (char*)base + a * sizeoftype, sizeoftype);
	memcpy((char*)base + a * sizeoftype, buf, sizeoftype);
	free(buf);
}


void QuickSort(void* a, int left, int right, int sizeoftype, int(*compare)(const void* a,const void* b))
{
	int r = right;
	int l = left;
	void* pivot = (void*)malloc(sizeoftype);
	memcpy(pivot, (void*)((char*)a + ((left + right)/2)*sizeoftype), sizeoftype);
	while(l < r)
	{
		while (compare(pivot, PositionInArray(a, l, sizeoftype)) > 0)
		{
			l++;
		}
		while (compare(PositionInArray(a, r, sizeoftype), pivot) > 0)
		{
			r--;
		}
		if (l <= r)
		{
			SwapVoid(a, l, r, sizeoftype);
			l++;
			r--;
		}
	}
	if (r > left)
	{
		QuickSort(a, left, r, sizeoftype, compare);
	}
	if (l < right)
	{
		QuickSort(a, l, right, sizeoftype, compare);
	}
}


void insertionSort(void * a, int l, int r, int sizeoftype, int(*compare)(const void* a, const void* b))
{
	int i;
	for (i = l + 1; i <= r; ++i)
	{
		int j = i - 1;
		void* v = (void*)malloc(sizeoftype);
		memcpy(v, (char*)a + i * sizeoftype, sizeoftype);
		while ((compare(PositionInArray(a, j, sizeoftype), v)>0) && ( j >= l ))
		{
			AssignValue(a, j+1, j, sizeoftype);
			--j;
		}
		memcpy((char*)a + (j + 1) * sizeoftype, v ,sizeoftype);
		free(v);
	}
}


void AssignValue(void* a, int i, int j, int sizeoftype)
{
	memcpy((char*)a+i*sizeoftype, (char*)a+j*sizeoftype, sizeoftype);
}


void ShellSort(int* a, int n)
{
	int i, j, k, temp;
	for(k = n / 2; k > 0; k /= 2)
	{
		for(i = k; i< n; i++)
		{
			temp = a[i];
			for(j = i; j >= k ;j-=k)
			{
				if(temp < a[j-k])
				{
					a[j] = a[j-k];
				}
				else
				{
					break;
				}
			}
			a[j] = temp;
		}
	}
}


void reload(int* arr1, int* arr2, int n)
{
	int i;
	for (i = 0; i < n; ++i)
	{
		arr2[i] = arr1[i];
	}
}


void QuickSortOpt(void* a, int n, int sizeoftype, int(*compare)(const void* a,const void* b))
{
	if (n<20)
	{
		insertionSort(a, 0, n-1, sizeoftype, compare);
	}
	else
	{
		QuickSort(a, 0, n - 1, sizeoftype, compare);
	}
}


void check(int* arr, int n, char* s, double time)
{
	int i;
	for (i = 0; i < n - 1; ++i)
	{
		if (arr[i] > arr[i + 1])
		{
			printf("Error in %s, %d", s, i);
			return;
		}
	}
	printf("\n");
	printf("%s is right, ",s);
	printf("time = %f sec\n",time);
}


void BheapCheckup(struct Bheap* h, int c )
{
	int p;
	for( p = c / 2;  p > 0 ; c = p , p = c / 2 )
	{
		if( h->data[p].key > h->data[c].key )
		{
			struct  Pair tmp = h->data[p];
			h->data[p] = h->data[c];
			h->data[c] = tmp;
		}
		else
		{
			break;
		}
	}
}
