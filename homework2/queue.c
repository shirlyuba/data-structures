#include<stdio.h>
#include<malloc.h>

typedef struct Node
{
	struct Node* next;
	struct Node* prev;
	int value;
} Node;

typedef struct Queue
{
	struct Node* first;
	struct Node* last;
	size_t size;
} Queue;


Queue* Queue_new();
void Queue_delete(Queue* l);
void push(Queue* l, int a);
int pop(Queue* l);
void print(Queue* Queue);
void create_Queue(Queue* l, int* a, int n);
int checkEmptyQueue(Queue* l);
int GetTop(Queue* l);


int main()
{
	int count;
	printf("enter the count of elements:\n");
	scanf("%d", &count);
	int* a = (int*)malloc(count*sizeof(int));
	printf("enter the values of elements: ");
	Queue* Q = Queue_new();
	create_Queue(Q, a, count);
	printf("your queue: ");
	print(Q);
	printf("top is : %d\n", GetTop(Q));
	pop(Q);
	printf("Queue without first element :");
	print(Q);
	printf("enter the element you want to add: ");
	int add;
	scanf("%d", &add);
	push(Q, add);
	print(Q);
	Queue_delete(Q);
	return 0;
}


Queue* Queue_new()
{
	Queue* empty = (Queue*)malloc(sizeof(Queue));
	empty->size = 0;
	empty->last = NULL;
	empty->first = NULL;
	return (empty);
}


void Queue_delete(Queue* l)
{
	Node* tmp = l->first;
	while(tmp)
	{
		Node* a = tmp;
		tmp = tmp->next;
		free(a);
	}
	l->first = NULL;
	l->last = NULL;
	l->size = 0;
}


int GetTop(Queue* l)
{
	return l->first->value;
}


int pop(Queue* l)
{
	if (l->first)
	{
		int tmp = l->first->value;
		Node* a = l->first->next;
		Node* b = l->first;
		if (a)
		{
			a->prev = NULL;
			l->first = a;
			b->prev = a;
			free(b);
			l->size--;
		}
		else
		{
			free(l->first);
			l->first = NULL;
			l->last = NULL;
		}
		return tmp;
	}
	return 0;
}


void print(Queue* Queue)
{
	if (Queue->first)
	{
		Node* tmp = Queue->first;
		while (tmp)
		{
			printf("%d ", tmp->value);
			tmp = tmp->next;
		}
		printf("\n");
	}
	else printf("queue is empty\n");
}


void push(Queue* l, int a)
{
	if (l->last)
	{
		Node* tmp = (Node*)malloc(sizeof(Node));
		l->last->next = tmp;
		tmp->prev = l->last;
		l->last = tmp;
		tmp->next = NULL;
		tmp->value = a;
	}
	else
	{
		l->last = (Node *)malloc(sizeof(Node));
		l->last->value = a;
		l->last->next = NULL;
		l->last->prev = NULL;
		l->first = l->last;
	}
	l->size++;
}


void create_Queue(Queue* l, int* a, int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		scanf("%d", &a[i]);
		push(l, a[i]);
	}
}


int checkEmptyQueue(Queue* l)
{
	if (l->first == NULL)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
