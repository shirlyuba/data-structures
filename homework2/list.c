#include<stdio.h>
#include<malloc.h>

typedef struct Node
{
	struct Node* next;
	struct Node* prev;
	int value;
} Node;

typedef struct List
{
	struct Node* first;
	struct Node* last;
	size_t size;
} TwoLinkedList;


TwoLinkedList* list_new();
void deleteLast(TwoLinkedList* l);
void list_delete(TwoLinkedList* l);
void push(TwoLinkedList* l, int a);
int pop(TwoLinkedList* l);
void unshift(TwoLinkedList* l, int a);
int shift(TwoLinkedList* l);
void print(TwoLinkedList* list);
void create_list(TwoLinkedList* l, int* a, int n);
int checkEmptyList(TwoLinkedList* l);
void reverse(TwoLinkedList* l);


int main()
{
	TwoLinkedList* l;
	l = list_new();
	int n;
	printf("enter the count of elements:\n");
	scanf("%d", &n);
	int* a = (int*)malloc(n*sizeof(int));
	printf("enter the elements which you want to add in list: ");
	create_list(l, a, n);
	printf("your list: ");
	print(l);
	printf("enter the element you want to add in the beginning of list: ");
	int element;
	scanf("%d", &element);
	unshift(l, element);
	print(l);
	reverse(l);
	printf("reverse list:\n");
	print(l);
	printf("the first element is %d\n", shift(l));
	printf("the last element is %d\n", pop(l));
	printf("Size of list is %d\n", l->size);
	list_delete(l);
	if (checkEmptyList(l))
	{
		printf("List was deleted");
	}
	else
	{
		printf("List was not deleted");
	}
	printf("\n");
	return 0;
}


TwoLinkedList* list_new()
{
	TwoLinkedList* empty = (TwoLinkedList*)malloc(sizeof(TwoLinkedList));
	empty->size = 0;
	empty->last = NULL;
	empty->first = NULL;
	return (empty);
}


void deleteLast(TwoLinkedList* l)
{
	if (l->last)
	{
		Node* last = l->last;
		l->last->prev->next = NULL;
		l->last = l->last->prev;
		free(last);
	}
}


void list_delete(TwoLinkedList* l)
{
	Node* tmp = l->first;
	while(tmp)
	{
		Node* a = tmp;
		tmp = tmp->next;
		free(a);
	}
	l->first = NULL;
	l->last = NULL;
	l->size = 0;
}


int pop(TwoLinkedList* l)
{
	if (l->last)
	{
		int tmp = l->last->value;
		Node* last = l->last;
		if (l->last->prev)
		{
			l->last->prev->next = NULL;
			l->last = l->last->prev;
			free(last);
		}
		else
		{
			free(l->last);
			l->last = NULL;
			l->first = NULL;
		}
		l->size--;
		return tmp;
	}
	else
	{
		return 0;
	}
}


void unshift(TwoLinkedList* l, int a)
{
	if (l->first)
	{
		Node* tmp = (Node*)malloc(sizeof(Node));
		l->first->prev = tmp;
		tmp->next = l->first;
		tmp->prev = NULL;
		tmp->value = a;
		l->first = tmp;
	}
	else
	{
		l->first = (Node*)malloc(sizeof(Node));
		l->first->value = a;
		l->first->next = NULL;
		l->first->prev = NULL;
		l->last = l->first;
	}
	l->size++;
}


int shift(TwoLinkedList* l)
{
	if (l->first)
	{
		int tmp = l->first->value;
		Node* a = l->first->next;
		Node* b = l->first;
		if (a)
		{
			a->prev = NULL;
			l->first = a;
			b->prev = a;
			free(b);
			l->size--;
		}
		else
		{
			free(l->first);
			l->first = NULL;
			l->last = NULL;
		}
		return tmp;
	}
	return 0;
}


 void reverse(TwoLinkedList* l)
{
	Node* current =l-> first;
	while (current != NULL)
	{
		Node* tmp = current->next;
		current->next = current->prev;
		current->prev = tmp;
		current = tmp;
	}
	current = l->last;
	l->last = l->first;
	l->first = current;
}


void print(TwoLinkedList* list)
{
	if (list->first)
	{
		Node* tmp = list->first;
		while (tmp)
		{
			printf("%d ", tmp->value);
			tmp = tmp->next;
		}
		printf("\n");
	}
	else
	{
		printf("list is empty");
	}
}


void push(TwoLinkedList* l, int a)
{
	if (l->last)
	{
		Node* tmp = (Node *)malloc(sizeof(Node));
		l->last->next = tmp;
		tmp->prev = l->last;
		l->last = tmp;
		tmp->next = NULL;
		tmp->value = a;
	}
	else
	{
		l->last = (Node*)malloc(sizeof(Node));
		l->last->value = a;
		l->last->next = NULL;
		l->last->prev = NULL;
		l->first = l->last;
	}
	l->size++;
}


void create_list(TwoLinkedList* l, int* a, int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		scanf("%d", &a[i]);
		push(l, a[i]);
	}
}


int checkEmptyList(TwoLinkedList* l)
{
	if (l->first == NULL)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
