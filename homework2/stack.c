#include<stdio.h>
#include<malloc.h>

typedef struct Node
{
	struct Node* next;
	struct Node* prev;
	int value;
} Node;

typedef struct Stack
{
	struct Node* first;
	struct Node* last;
	size_t size;
} Stack;


Stack* Stack_new();
void Stack_delete(Stack* l);
void push(Stack* l, int a);
int pop(Stack* l);
int GetTop(Stack* l);
void print(Stack* Stack);
void create_Stack(Stack* l, int* a, int n);
int checkEmptyStack(Stack* l);

int main()
{
	int count;
	printf("enter the count of elements:\n");
	scanf("%d", &count);
	int* a = (int*)malloc(count*sizeof(int));
	printf("enter their values: ");
	Stack* S = Stack_new();
	create_Stack(S, a, count);
	printf("your stack: ");
	print(S);
	printf("top is : %d\n", GetTop(S));
	pop(S);
	printf("Stack without last element: ");
	print(S);
	printf("enter the element you want to add: ");
	int add;
	scanf("%d", &add);
	push(S, add);
	print(S);
	Stack_delete(S);
	return 0;
}


Stack* Stack_new()
{
	Stack* empty = (Stack*)malloc(sizeof(Stack));
	empty->size = 0;
	empty->last = NULL;
	empty->first = NULL;
	return (empty);
}


int GetTop(Stack* l)
{
	if (l->first)
	{
		return l->first->value;
	}
	return 0;
}


void Stack_delete(Stack* l)
{
	Node* tmp = l->first;
	while(tmp)
	{
		Node* a = tmp;
		tmp = tmp->next;
		free(a);
	}
	l->first = NULL;
	l->last = NULL;
	l->size = 0;
}


void push(Stack* l, int a)
{
	if (l->first)
	{
		Node* tmp = (Node*)malloc(sizeof(Node));
		l->first->prev = tmp;
		tmp->next = l->first;
		tmp->prev = NULL;
		tmp->value = a;
		l->first = tmp;
	}
	else
	{
		l->first = (Node*)malloc(sizeof(Node));
		l->first->value = a;
		l->first->next = NULL;
		l->first->prev = NULL;
		l->last = l->first;
	}
	l->size++;
}


int pop(Stack* l)
{
	if (l->first)
	{
		int tmp = l->first->value;
		Node* a = l->first->next;
		Node* b = l->first;
		if (a)
		{
			a->prev = NULL;
			l->first = a;
			b->prev = a;
			free(b);
			l->size--;
		}
		else
		{
			free(l->first);
			l->first = NULL;
			l->last = NULL;
		}
		return tmp;
	}
	return 0;
}


void print(Stack* Stack)
{
	if (Stack->first)
	{
		Node* tmp = Stack->first;
		while (tmp)
		{
			printf("%d ", tmp->value);
			tmp = tmp->next;
		}
		printf("\n");
	}
	else
	{
		printf("Empty stack\n");
	}
}


void create_Stack(Stack* l, int* a, int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		scanf("%d", &a[i]);
		push(l, a[i]);
	}
}


int checkEmptyStack(Stack* l)
{
	if (l->first == NULL)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

