#include<stdio.h>
#include<malloc.h>
#include<time.h>
#include<stdlib.h>


void ShellSort(int* a, int n)
{
	int i, j, k, temp;
	for(k = n / 2; k > 0; k /= 2)
	{
		for(i = k; i< n; i++)
		{
			temp = a[i];
			for(j = i; j >= k ;j-=k)
			{
				if(temp < a[j-k])
				{
					a[j] = a[j-k];
				}
				else
				{
					break;
				}
			}
			a[j] = temp;
		}
	}
}


void GetMassInt(int n,int *a)
{
	int i;
	srand(time(NULL));
	for (i = 0; i < n; i++)
	{
		a[i] = rand();
	}
}


int check(int * a, int n)
{
	int i;
	for (i = 0; i < n - 1; i++)
	{
		if (a[i] > a[i+1])
		{
			return 0;
		}
	}
	return 1;
}


int main()
{
	int n;
	printf("Enter the count of elements: ");
	scanf("%d", &n);
	int* a = (int*)malloc(n * sizeof(int));
	GetMassInt(n, a);
	ShellSort(a,n);
	if (check(a, n))
	{
		printf("true");
	}
	else
	{
		printf ("false");
	}
	printf("\n");
	free(a);
	return 0;
}
