#include "LongArifmetic.h"

BigInt::BigInt()
{
  _size = 0;
  _sign = 1;
  _arr.assign(0, 0);
}
BigInt::BigInt(int num)
{
  _size = 0;
  if (num >= 0)
    _sign = 1;
  else{
    _sign = -1;
    num *= -1;
  }
  _size = 0;
  while (num != 0)
  {
    _arr.push_back(num % BASE);
    num /= BASE;
    ++_size;
  }
}

BigInt::BigInt(long long num)
{
  if (num >= 0)
    _sign = 1;
  else{
    _sign = -1;
    num *= -1;
  }
  _size = 0;
  while (num != 0)
  {
    _arr.push_back(num % BASE);
    num /= BASE;
    ++_size;
  }
}

BigInt::BigInt(std::string str)
{
  _sign = 1;
  _size = 0;
  if (str[0] == '-'){
    _sign = -1;
    for (int i = str.length() - 1; i > 0; --i){
      _arr.push_back(str[i] - '0');
      ++_size;
    }
  }
  else
  {
    for (int i = str.length() - 1; i >= 0; --i){
      _arr.push_back(str[i] - '0');
      ++_size;
    }
  }
}

BigInt::BigInt(const BigInt & bint) : _size(bint._size), _sign(bint._sign), _arr(bint._arr){}

void BigInt::operator= (const BigInt & bint)
{
  _size = bint._size;
  _sign = bint._sign;
  _arr = bint._arr;
}

BigInt BigInt::operator-() const
{
  BigInt res;
  res._arr = _arr;
  res._sign = -1 * _sign;
  res._size = _size;
  return res;
}

std::ostream & operator<<(std::ostream & os, const BigInt & bint) 
{
  if (bint._size == 0){
    std::cout << "0";
    return os;
  }
  if (bint._sign < 0)
    std::cout << "-";
  for (int i = bint._size - 1; i >= 0; --i)
    std::cout << (int)bint._arr[i]; 
  return os;
}

std::istream & operator>>(std::istream & is, BigInt & bint)
{
  std::string s;
  std::cin >> s;
  bint = BigInt(s);
  return is;
}

BigInt BigInt::operator+(const BigInt & bint) const
{
  BigInt res;
  if (_sign * bint._sign == 1)
  {
    res._sign = _sign;
    res._size = (_size > bint._size) ? _size : bint._size;
    res._arr.assign(res._size, 0);
    int rest = 0;
    for (int i = 0; i < res._size; ++i)
    {
      char comp1 = (_size > i) ? _arr[i] : 0;
      char comp2 = (bint._size > i) ? bint._arr[i] : 0;
      res._arr[i] = (comp1 + comp2 + rest) % BASE;
      rest = (comp1 + comp2 + rest) / (BASE);
    }
    if (rest == 1){
      res._arr.push_back(1);
      ++res._size;
    }
    return res;
  }
  if (bint._sign == -1)
  {
    return *this - (-bint);
  }
  if (this->_sign == -1)
    return -(bint - (-*this));
}

BigInt operator-(const BigInt & a, const BigInt & b)
{
  if ((a._sign > 0) && (b._sign < 0))
    return a + (-b);
  if ((a._sign < 0) && (b._sign > 0))
    return -((-a) + b);
  if ((a._sign < 0) && (b._sign < 0))
    return -((-b) - (-a));
  if ((a._sign > 0) && (b._sign > 0))
  {
    if (a < b)
      return -(b - a);
    if (a >= b)
    {
      BigInt res;
      std::vector<char> arr1, arr2;
      arr1 = a._arr;
      arr2 = b._arr;
      for (int i = 0; i < a._size; ++i)
      {
        char comp2 = (b._size > i) ? arr2[i] : 0;
        if (arr1[i] < comp2)
        {
          int k = i + 1;
          while (arr1[k] == 0) ++k;
          --arr1[k];
          for (int j = k - 1; j > i; --j)
            arr1[j] += BASE - 1;
          arr1[i] += BASE;
        }
        res._arr.push_back(arr1[i] - comp2);
        ++res._size;
      }
      int k = res._size - 1;
      while ((res._arr[k] == 0) && (k > 0))
      {
        res._arr.erase(res._arr.end() - 1);
        --res._size;
        --k;
      }
      return res;
    }
  }
}

bool operator>(const BigInt & a, const BigInt & b)
{
  if ((a._sign > 0) && (b._sign <= 0))
    return 1;
  if ((a._sign <= 0) && (b._sign > 0))
    return 0;
  if ((a._sign > 0) && (b._sign > 0))
  {
    if (a._size > b._size)
      return 1;
    if (a._size < b._size)
      return 0;
    int k = a._size - 1;
    while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
    if (k == -1)
      return 0;
    if (a._arr[k] > b._arr[k])
      return 1;
    else
      return 0;
  }
  if ((a._sign < 0) && (b._sign < 0))
  {
    if (a._size > b._size)
      return 0;
    if (a._size < b._size)
      return 1;
  }
  int k = a._size - 1;
  while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
  if (k == -1)
    return 0;
  if (a._arr[k] > b._arr[k])
    return 1;
  else
    return 0;
}

bool operator>=(const BigInt & a, const BigInt & b)
{
  if ((a._sign > 0) && (b._sign <= 0))
    return 1;
  if ((a._sign <= 0) && (b._sign > 0))
    return 0;
  if ((a._sign > 0) && (b._sign > 0))
  {
    if (a._size > b._size)
      return 1;
    if (a._size < b._size)
      return 0;
    int k = a._size - 1;
    while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
    if (k == -1)
      return 1;
    if (a._arr[k] > b._arr[k])
      return 1;
    else
      return 0;
  }
  if ((a._sign < 0) && (b._sign < 0))
  {
    if (a._size > b._size)
      return 0;
    if (a._size < b._size)
      return 1;
  }
  int k = a._size - 1;
  while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
  if (k == -1)
    return 1;
  if (a._arr[k] < b._arr[k])
    return 1;
  else
    return 0;
}

bool operator<(const BigInt & a, const BigInt & b)
{
  if ((a._sign > 0) && (b._sign < 0))
    return 0;
  if ((a._sign < 0) && (b._sign > 0))
    return 1;
  if ((a._sign > 0) && (b._sign > 0))
  {
    if (a._size > b._size)
      return 0;
    if (a._size < b._size)
      return 1;
    int k = a._size - 1;
    while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
    if (k == -1)
      return 0;
    if (a._arr[k] < b._arr[k])
      return 1;
    else
      return 0;
  }
  if ((a._sign < 0) && (b._sign < 0))
  {
    if (a._size < b._size)
      return 0;
    if (a._size > b._size)
      return 1;
  }
  int k = a._size - 1;
  while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
  if (k == -1)
    return 0;
  if (a._arr[k] > b._arr[k])
    return 1;
  else
    return 0;
}

bool operator<=(const BigInt & a, const BigInt & b)
{
  if ((a._sign > 0) && (b._sign <= 0))
    return 1;
  if ((a._sign <= 0) && (b._sign > 0))
    return 0;
  if ((a._sign > 0) && (b._sign > 0))
  {
    if (a._size > b._size)
      return 1;
    if (a._size < b._size)
      return 0;
    int k = a._size - 1;
    while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
    if (k == -1)
      return 1;
    if (a._arr[k] > b._arr[k])
      return 1;
    else
      return 0;
  }
  if ((a._sign < 0) && (b._sign < 0))
  {
    if (a._size > b._size)
      return 0;
    if (a._size < b._size)
      return 1;
  }
  int k = a._size - 1;
  while ((k >= 0) && (a._arr[k] == b._arr[k])) --k;
  if (k == -1)
    return 1;
  if (a._arr[k] < b._arr[k])
    return 1;
  else
    return 0;
}

bool operator==(const BigInt & a, const BigInt & b)
{
  if ((a._sign != b._sign) || (a._size != b._size))
    return 0;
  for (int i = 0; i < a._size; ++i)
  if (a._arr[i] != b._arr[i])
    return 0;
  return 1;
}

bool operator!=(const BigInt & a, const BigInt & b)
{
  if ((a._sign != b._sign) || (a._size != b._size))
    return 1;
  for (int i = 0; i < a._size; ++i)
  if (a._arr[i] != b._arr[i])
    return 1;
  return 0;
}

BigInt operator*(const BigInt & a, const BigInt & b)//Karatsuba
{
  if ((a._size == 0) || (b._size == 0))
    return BigInt(0);
  int ka = 0, kb = 0;
  for (int i = 0; i < a._size; ++i) ka += a._arr[i];
  for (int i = 0; i < b._size; ++i) kb += b._arr[i];
  if (ka == 0 || kb == 0)
    return BigInt(0);
  if ((a._size == 1) && (b._size == 1))
    return BigInt(a._arr[0] * b._arr[0]);
  int divideSize = (a._size > b._size) ? a._size : b._size;
  if (divideSize % 2 == 1)
    ++divideSize;
  divideSize /= 2;
  BigInt a1, a2, b1, b2;
  for (int i = 0; i < divideSize; ++i)
  {
    if (a._size > i){
      a1._arr.push_back(a._arr[i]);
      ++a1._size;
    }
    if (b._size > i){
      b1._arr.push_back(b._arr[i]);
      ++b1._size;
    }
    if (a._size > divideSize + i){
      a2._arr.push_back(a._arr[divideSize + i]);
      ++a2._size;
    }
    if (b._size > divideSize + i){
      b2._arr.push_back(b._arr[divideSize + i]);
      ++b2._size;
    }
  }
  BigInt res;
  BigInt comp1, comp2, comp3;
  comp1 = a1 * b1;
  comp2 = ((a1 + a2) * (b1 + b2) - (a1*b1 + a2*b2));
  if (comp2._size != 0){
    comp2._arr.insert(comp2._arr.begin(), divideSize, 0);
    comp2._size += divideSize;
  }
  comp3 = a2 * b2;
  if (comp3._size != 0){
    comp3._arr.insert(comp3._arr.begin(), 2 * divideSize, 0);
    comp3._size += divideSize * 2;
  }
  res = comp1 + comp2 + comp3;
  res._sign = a._sign * b._sign;
  return res;
}

BigInt operator/ (const BigInt & a, const BigInt & b)
{
  int degree = a._size - b._size;
  if (degree < 0)
    return 0;
  BigInt res;
  BigInt rest = a;
  BigInt divisor = b;
  divisor._arr.insert(divisor._arr.begin(), degree , 0);
  divisor._size += degree;
  int count = 0;
  for (int i = 0; i <= degree; ++i)
  {
    if (i != 0){
      divisor._arr.erase(divisor._arr.begin(), divisor._arr.begin() + 1);
      --divisor._size;
    }
    count = 0;
    while (rest >= divisor)
    {
      rest = rest - divisor;
      ++count;
    }
    if (i + count != 0)
    {
      res._arr.insert(res._arr.begin(),1, count);
      ++res._size;
    }
  }
  return res;
}

BigInt operator%(const BigInt & a, const BigInt & b)
{
  int degree = a._size - b._size;
  if (degree < 0)
    return a;
  if (a < b)
    return a;
  BigInt res;
  BigInt rest = a;
  BigInt divisor = b;
  divisor._arr.insert(divisor._arr.begin(), degree, 0);
  divisor._size += degree;
  for (int i = 0; i <= degree; ++i)
  {
    if (i != 0){
      divisor._arr.erase(divisor._arr.begin(), divisor._arr.begin() + 1);
      --divisor._size;
    }
    while (rest >= divisor)
    {
      rest = rest - divisor;
    }
  }
  return rest;
}
