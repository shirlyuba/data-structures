#include <iostream>
#include <string>

using namespace std;

static const int MAX_GAME_OBJECTS = 100;

class GameObject
{
public:												
	int x, y;
	GameObject(int x1, int y1, int size1) 
	{
		x = x1;
		y = y1;
		_size = size1;
	}
	void move1(int new_x, int new_y)									
	{
		x = new_x;
		y = new_y;
	}
	virtual char symbol() const { return '?'; }						
	virtual void print() const {};

	virtual ~GameObject() {};

	int _size;
};


class Creature : public GameObject
{
public:
	int _hit_points;											
	Creature(int hit_points, int x, int y, int size1 = 1) : _hit_points(hit_points), GameObject(x, y, size1) {}
	virtual char symbol() const { return '?'; }					
	virtual void print() const {};								

	virtual ~Creature() {};
};


class Hero : public Creature
{
public:
	string _name;												
	Hero(string name, int hit_points, int x, int y, int size1) : _name(name), Creature(hit_points, x, y, size1) {}
	virtual char symbol() const { return _name[0]; }			
	virtual void print() const									
	{
		cout << "Great hero " << _name << " with hp " << _hit_points << " at (" << x << "," << y << ")" << endl;
	}

	virtual ~Hero() {};
};


class Monster : public Creature
{
public:
	Monster(int hit_points, int x, int y, int size1) : Creature(hit_points, x, y, size1) {}
	virtual char symbol() const { return 'm'; }					
	virtual void print() const									
	{
		cout << "Some monster with hp " << _hit_points << " at (" << x << "," << y << ")" << endl;
	}

	virtual ~Monster() {};
};

class Terrain : public GameObject
{
public:
	Terrain(int x, int y, int size1) : GameObject(x, y, size1) {}
	virtual char symbol() const { return '?'; }					
	virtual void print() const {};								

	virtual ~Terrain() {};
};


class Lake : public Terrain
{
public:
	int _depth;

	Lake(int x, int y, int size1, int depth) : Terrain(x, y, size1), _depth(depth) {}
	virtual char symbol() const { return 'O'; }					
	virtual void print() const									
	{
		cout << "Clean lake of depth " << _depth << " at (" << x << "," << y << ") size " << _size << endl;
	}

	virtual ~Lake() {};
};


class Forest : public Terrain
{
public:
	Forest(int x, int y, int size1) : Terrain(x, y, size1) {}
	virtual char symbol() const { return 'F'; }					
	virtual void print() const									
	{
		cout << "Forest at (" << x << "," << y << ") size " << _size << endl;
	}

	virtual ~Forest() {};
};


class Mountain : public Terrain
{
public:
	int _high;

	Mountain(int x, int y, int size1, int high) : Terrain(x, y, size1), _high(high) {}
	virtual char symbol() const { return '^'; }					
	virtual void print() const									
	{
		cout << "Big mountain " << _high << " feet high at (" << x << "," << y << ") size " << _size << endl;
	}

	virtual ~Mountain() {};
};



class World
{
private:
	int _width, _height;
	char** map1;												
	GameObject* objects[MAX_GAME_OBJECTS];						 
	World(const World&);										
	void operator= (const World&);								
	void render(int key, int a);								

public:
	int objects_count;											

	World(int width, int height) : _width(width), _height(height)  
	{
		map1 = new char*[_height];
		for (int i = 0; i < height; i++)
		map1[i] = new char[_width];
		objects_count = 0;
		render(0, 0);
	}
	void show() const;										
	void add_object(GameObject* new_object);			
	void worldPrint();								        	
	void worldMove(int i, int new_x, int new_y);		

	~World()											
	{
		for (int i = 0; i < objects_count; i++)
			delete objects[i];
        for (int i = 0; i < _width; i++)
        {
            delete map1[i];
        }
        delete map1;
	}
};

void World::render(int key, int a)
{
	if (key == 0)
	{
		for (int i = 0; i < _height; i++)
		{
            for (int j = 0; j < _width; j++)
            {
                map1[i][j] = '.';
            }
		}
	}
	else
	{
	if (key == 1)
	{
        for (int j = objects[a]->x - 1; j < objects[a]->x + objects[a]->_size - 1; j++)
        {
            for (int i = objects[a]->y - 1; i < objects[a]->y + objects[a]->_size - 1; i++)
            {
                map1[j][i] = objects[a]->symbol();
            }
        }
    }
    else
    {
        for (int j = objects[a]->x - 1; j < objects[a]->x + objects[a]->_size - 1; j++)
        {
            for (int i = objects[a]->y - 1; i < objects[a]->y + objects[a]->_size - 1; i++)
            {
                map1[j][i] = '.';
            }
        }
    }
    }
}

void World::show() const
{
	int i, j;
	for( i = 0; i < _height; i++)
	{
		for (j = 0; j < _width; j++)
		 cout << map1[i][j] << " ";
		 cout << endl;
	}
}

void World::add_object(GameObject* new_object)
{
	objects[objects_count] = new_object;
	objects_count++;
	render(1, objects_count - 1);
}

void World::worldPrint()
{
	cout << "Dumping ";
	cout << objects_count;
	cout << " objects:" << endl;
	for (int i = 0; i < objects_count; i++)
	{
		cout << i + 1 << ". ";
		objects[i]->print();
	}
}

void World::worldMove(int i, int new_x, int new_y)
{
    i = i - 1;
    render(-1, i);
	objects[i]->move1(new_x, new_y);
	render(1, i);
}

void help()												
{
	cout << "1. If you want to create new map: map width height" << endl;
	cout << "2. If you want to create new objects:" << endl;
	cout << "Lake: create lake depth _ at x y size _" << endl;
	cout << "Mountain: create mountain height __ at x y size __" << endl;
	cout << "Forest: create forest at x y size __" << endl;
	cout << "Hero: create hero NAME at x y  hp __" << endl;
	cout << "Monster: create monster at x y hp __" << endl;
	cout << "3. If you want to move objects: move 'number of object' 'new x' 'new y'" << endl;
	cout << "4. If you want to see created objects: dump" << endl;
	cout << "5. If you want to see the map: show" << endl;
	cout << "6. If you want to see help page: help" << endl;
	cout << "7. If you want to exit: quit" << endl;
}



int main()
{
	GameObject* ob[MAX_GAME_OBJECTS];
	string s;											
	int width = 0, height = 0;
	cout << "create map:" << endl << "> ";
	cin >> s;
	cin >> width >> height;
	World* world = new World(width, height);

	cout << "enter command:" << endl << "> ";
	cin >> s;
	cout << endl;

	int depth, high, x, y, size1, hit_points;
	string name = "";
	int j = 0;
	while (s != "quit")
	{
		switch (s[0])
		{
		case 's': {										
			world->show();
			break;
		}
		case 'c': {										
			cin >> s;
			depth = 0; high = 0; x = 0; y = 0; size1 = 0; hit_points = 0;
			name = "";
			switch (s[0])
			{
			case 'l': {				
				cin >> s >> depth;
				cin >> s >> x >> y;
				cin >> s >> size1;
				break; }
			case 'm': {
				if (s[2] == 'u')
				{
					cin >> s >> high;
					cin >> s >> x >> y;
					cin >> s >> size1;
				}
				else
				{
					cin >> s >> x >> y;
					cin >> s >> hit_points;
				}
				break;
			}
			case 'f': {	
				cin >> s >> x >> y;
				cin >> s >> size1;
				break;
			}
			case 'h': {
				cin >> name;
				cin >> s >> x >> y;
				cin >> s >> hit_points;
				break;
			}
			}
			if (hit_points != 0)
			{
				if (name != "")
				{
					ob[j] = new Hero(name, hit_points, x, y, 1);
					world->add_object(ob[j]);
				}
				else
				{
					ob[j] = new Monster(hit_points, x, y, 1);
					world->add_object(ob[j]);
				}
			}
			else if (high != 0)
			{
				ob[j] = new Mountain(x, y, size1, high);
				world->add_object(ob[j]);
			}
			else if (depth != 0)
			{
				ob[j] = new Lake(x, y, size1, depth);
				world->add_object(ob[j]);
			}
			else
			{
				ob[j] = new Forest(x, y, size1);
				world->add_object(ob[j]);
			}
			j++;
			break;
		}

		case 'd': {												
			world->worldPrint();
			break;
		}

		case 'm': {												
			int num;
			cin >> num >> x >> y;
			world->worldMove(num, x, y);
			break;
		}
		case 'h': {												
			help();
			break;
		}
		}
		cout << "enter command:" << endl << "> ";
		cin >> s;
		cout << endl;
	}
	cout << "Bye!";
	delete world;
	return 0;
}
