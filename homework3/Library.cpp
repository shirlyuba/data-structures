#include<vector>
#include<iostream>
#include<string>
#include<set>
#include<algorithm>


using namespace std;


class edition
{
public:
	void reduceCount()
	{
		count--;
	}
	void increaseCount()
	{
		count++;
	}
	int getCount()
	{
		return count;
	}
	void setCount(int C)
	{
		count = C;
		general_count = C;
	}
	int getGeneralCount()
	{
		return general_count;
	}
	bool onHands = false;
	virtual ~edition() {};

private:
	int count;
	int general_count;
};

class book : public edition
{
public:
	void setName(string S)
	{
		name = S;
	}
	void setAuthors(string x)
	{
		authors.push_back(x);
	}
	void setPublishment(string P)
	{
		publishment = P;
	}
	void setYear(int Y)
	{
		year = Y;
	}
	void setPages(int Pag)
	{
		pages = Pag;
	}
	void showInfo()
	{
		showAuthor();
		showName();
		showPublishment();
		showYear();
		showPage();
		showAvailableCount();
	}
	void showAvailableCount()
	{
		cout << "we have " << getCount() << " these books" << endl;
	}
	void showAuthor()
	{
		cout << "authors of book: ";
		for (int i = 0; i < authors.size(); i++)
		{
			cout << authors[i] << " ";
		}
		cout << endl;
	}
	void showName()
	{
		cout << "name of book: " << name << endl;
	}
	void showPublishment()
	{
		cout << "publishment of book: " << publishment << endl;
	}
	void showYear()
	{
		cout << "year of public: " << year << endl;
	}
	void showPage()
	{
		cout << "count of pages: " << pages << endl;
	}
	string getName()
	{
		return name;
	}
	string getPublishment()
	{
		return publishment;
	}
	int getYear()
	{
		return year;
	}
	vector <string> getAuthors()
	{
		return authors;
	}
private:
	vector <string> authors;
	string name;
	string publishment;
	int year;
	int pages;
};



class collectedWorks : public edition
{
public:
	void addBook(book* B)
	{
		books.push_back(B);
	}
	void popBook(book* A)
	{
		books.erase(find(books.begin(), books.end(), A));
	}
	void setNameOfWorks(string S)
	{
		nameOfWorks = S;
	}
	int countOfToms()
	{
		return books.size();
	}
	void showAvailableCount()
	{
		cout << "we have " << getCount() << " these collected works" << endl;
	}
	void showNameOfWorks()
	{
		cout << "name of works: ";
		cout << nameOfWorks << endl;
	}
	void showCountOfToms()
	{
		cout << "count of toms: ";
		cout << countOfToms() << endl;
	}
	void showAuthors()
	{
		books.back()->showAuthor();
	};
	void showInfo()
	{
		showNameOfWorks();
		showCountOfToms();
		showAuthors();
		showAvailableCount();
	}
	string getName()
	{
		return nameOfWorks;
	}
private:
	vector <book*> books;
	string nameOfWorks;
};




class magazine : public edition
{
public:
	void setNameOfMag(string S)
	{
		name = S;
	}
	void showAvailableCount()
	{
		cout << "we have " << getCount() << " these magazines" << endl;
	}
	void setPeriod(string P)
	{
		period = P;
	}
	void showName()
	{
		cout << "name of Magazine: ";
		cout << name << endl;
	}
	void showPeriod()
	{
		cout << "the period of Magazine is ";
		cout << period << endl;
	}
	void showInfo()
	{
		showName();
		showPeriod();
		showAvailableCount();
	}
	string getName()
	{
		return name;
	}
private:
	string name;
	string period;
};



class library
{
public:
	void addBook(book* A)
	{
		books.push_back(A);
	}
	void addWorks(collectedWorks* B)
	{
		works.push_back(B);
	}
	void addMagazine(magazine* C)
	{
		magazines.push_back(C);
	}
	void deleteFromLibrary(edition* A)
	{
		if (dynamic_cast <book*> (A) != NULL)
			books.erase(find(books.begin(), books.end(), A));
		if (dynamic_cast <collectedWorks*> (A) != NULL)
			works.erase(find(works.begin(), works.end(), A));
		if (dynamic_cast <magazine*> (A) != NULL)
			magazines.erase(find(magazines.begin(), magazines.end(), A));
	}
	void giveOnHand(edition* A)
	{
		if (A->getCount() != 0)
		{
			if (dynamic_cast <book*> (A) != NULL)
				cout << "you take this book" << endl;
			if (dynamic_cast <collectedWorks*> (A) != NULL)
				cout << "you take this collected works" << endl;
			if (dynamic_cast <magazine*> (A) != NULL)
				cout << "you take this magazine" << endl;
			A->reduceCount();
			(*A).onHands = true;
		}
		else
		{
			if (dynamic_cast <book*> (A) != NULL)
				cout << "all these books is already on hands" << endl;
			if (dynamic_cast <collectedWorks*> (A) != NULL)
				cout << "collected works is already on hands" << endl;
			if (dynamic_cast <magazine*> (A) != NULL)
				cout << "magazine is already on hands" << endl;
		}	
	}
	void getTitleOfOnHands()
	{
		cout << "next books and magazines are on hands right now: " << endl;
		for (auto it : books)
		{
			if ((*it).onHands == true)
				it->showName();
		}
		for (auto it : works)
		{
			if ((*it).onHands == true)
				it->showNameOfWorks();
		}
		for (auto it : magazines)
		{
			if ((*it).onHands == true)
				it->showName();
		}
	}
	void returnPublication(book* A)
	{
		cout << "the book " << (*A).getName() << " is returned" << endl;
		A->increaseCount();
		if (A->getGeneralCount() == A->getCount())
		{
			(*A).onHands = false;
		}
	}
	void returnPublication(magazine* A)
	{
		cout << "the magazine " << (*A).getName() << " is returned" << endl;
		A->increaseCount();
		if (A->getGeneralCount() == A->getCount())
		{
			(*A).onHands = false;
		}
	}
	void returnPublication(collectedWorks* A)
	{
		cout << "the collected works " << (*A).getName() << " is returned" << endl;
		A->increaseCount();
		if (A->getGeneralCount() == A->getCount())
		{
			(*A).onHands = false;
		}
	}
	void findName(string S)
	{
		for (auto it : books)
		{
			if (it->getName() == S)
				it->showInfo();
		}
		for (auto it : works)
		{
			if (it->getName() == S)
				it->showInfo();
		}
		for (auto it : magazines)
		{
			if (it->getName() == S)
				it->showInfo();
		}
	}
	void findPublication(string S)
	{
		for (auto it : books)
		{
			if (it->getPublishment() == S)
				it->showInfo();
		}
	}
	void findYear(int Y)
	{
		for (auto it : books)
		{
			if (it->getYear() == Y)
				it->showInfo();
		}
	};
	void findAuthor(string A)
	{
		for (auto it : books)
		{
			for (int i = 0; i < it->getAuthors().size(); i++)
			{
				if (it->getAuthors()[i] == A)
				{
					it->showInfo();
				}
			}
		}
	}
private:
	vector <book*> books;
	vector <collectedWorks*> works;
	vector <magazine*> magazines;
};



int main()
{
	book* book1 = new book();
	book1->setAuthors("Tolstoi");
	book1->setAuthors("Gogol");
	book1->setName("war and peace tom 1");
	book1->setPublishment("russian true");
	book1->setYear(1889);
	book1->setPages(350);
	book1->setCount(3);
	book1->showInfo();
	cout << "_______________" << endl;
	book* book2 = new book();
	book2->setAuthors("Tolstoi");
	book2->setAuthors("Gogol");
	book2->setName("war and peace tom 2");
	book2->setPublishment("BBC");
	book2->setYear(1895);
	book2->setCount(1);
	book2->setPages(500);
	book2->showInfo();
	cout << "_______________" << endl;
	book* book3 = new book();
	book3->setAuthors("Mistery");
	book3->setName("DARKness");
	book3->setPublishment("BBC");
	book3->setYear(1995);
	book3->setPages(100);
	book3->setCount(2);
	book3->showInfo();
	cout << "_______________" << endl;
	magazine* magazine1 = new magazine();
	magazine1->setNameOfMag("MAYFAIR");
	magazine1->setPeriod("month");
	magazine1->setCount(1);
	magazine1->showInfo();
	cout << "_______________" << endl;
	collectedWorks* collections1 = new collectedWorks();
	collections1->addBook(book1);
	collections1->addBook(book2);
	collections1->setNameOfWorks("WAR AND PEACE");
	collections1->setCount(2);
	collections1->showInfo();
	cout << "_______________" << endl;
	library* library1 = new library();
	library1->addWorks(collections1);
	library1->addBook(book3);
	library1->addBook(book1);
	library1->addMagazine(magazine1);
	library1->findAuthor("Gogol");
	cout << "_______________" << endl;
	library1->deleteFromLibrary(book1);
	library1->findAuthor("Gogol");
	cout << "_______________" << endl;
	library1->findYear(1995);
	cout << "_______________" << endl;
	library1->findPublication("BBC");
	cout << "_______________" << endl;
	library1->giveOnHand(book3);
	cout << "_______________" << endl;
	library1->giveOnHand(magazine1);
	cout << "_______________" << endl;
	library1->giveOnHand(collections1);
	cout << "_______________" << endl;
	library1->getTitleOfOnHands();
	cout << "_______________" << endl;
	library1->returnPublication(book3);
	cout << "_______________" << endl;
	library1->getTitleOfOnHands();
	cout << "_______________" << endl;
	library1->findName("DARKness");
	delete book1;
	delete book2;
	delete book3;
	delete collections1;
	delete magazine1;
	delete library1;
	return 0;
}