#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

void findWords(string s, string& word1, string& word2)
{
	int first = s.find(" ");
	string r = s.substr(first + 1);
	int last = r.find(" ");
	word1 = s.substr(first + 1, last);
	word2 = r.substr(last + 1);
}


void changeStr(string& s, string word1, string word2)
{
	while ((s.find(word1) != string::npos) && (s.find("#define") == string::npos))
	{
		auto pos = s.find(word1);
		s.replace(pos, word1.length(), word2);
	}
}

void deleteCom(string& s)
{
	if (s.find('/') != string::npos)
	{
		auto first = s.find('/');
		s.erase(first, 1);
		auto last = s.find('/');
		s.c_str();
		if ((s[first - 1] != '"') && (s[last + 1] != '"'))
		{
			s.erase(first, last - first + 1);
		}
		else s.insert(first, "/");
	}
};

void deleteCom1(string& s)
{
	if (s.find("//") != string::npos)
	{
		auto first = s.find("//");
		s.erase(first, s.length() - first + 1);
	}
}

void pushString(ofstream &f2, string s)
{
	f2 << s << endl;
}

int main()
{
	ifstream f("input.cpp");
	ofstream f2("output.cpp");
	vector <string> file;
	vector <pair <string, string> > words;
	string s;
	int k = 0;
	while (getline(f,s))
	{
		if (s.find("#define") != string::npos)
		{
			string word1, word2;
			findWords(s, word1, word2);
			words.push_back({ word1, word2 });
			k++;
		}
		for (int i = 0; i < k; i++)
		{
			changeStr(s, words[i].first, words[i].second);
		}
		deleteCom1(s);
		deleteCom(s);
		pushString(f2, s);
	}
	return 0;
}
