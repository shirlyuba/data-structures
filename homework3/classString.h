#ifndef CLASSSTRING_H
#define CLASSSTRING_H

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string.h>
#include <new>

class Str
{
public:
  Str() : len_(0), arr_(NULL){}
  Str(char* s);
  Str(std::string & s);
  Str(const Str & s);
  void operator=(const Str & s);
  friend Str & operator+(Str & s1, Str & s2);
  friend bool operator<(Str & s1, Str & s2);
  friend bool operator>(Str & s1, Str & s2);
  friend bool operator==(Str & s1, Str & s2);
  friend bool operator!=(Str & s1, Str & s2);
  friend std::ostream & operator<<(std::ostream & os, Str & s);
  friend std::istream & operator>>(std::istream & is, Str & s);
  char* operator[] (int index);
  void deleteLast();
  void append(const Str & s);
  void insert(Str s, int index);
  void erase(int index, int num);
  ~Str() {delete [] arr_;};
private:
  char* arr_;
  size_t len_;
  size_t capacity_;
};

#endif
