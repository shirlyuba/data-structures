#ifndef METRIC_H
#define METRIC_H
#define _USE_MATH_DEFINES
#include <iostream>
#include <vector>
#include <limits.h>
#include <float.h>
#include <math.h>


double getDistance(std::pair <double, double>, std::pair <double, double>);
double det(std::vector <std::pair <double, double> >, std::vector <std::pair <double, double> >);
std::pair <double, double> getCoordOfVector(std::vector <std::pair <double, double> >);
std::vector <std::pair <double, double> > createVector(std::pair <double, double>, std::pair <double, double>); /*may use for creating segments*/
double myCos(std::vector <std::pair <double, double> >, std::vector <std::pair <double, double> >);
double scalMultiple(std::vector <std::pair <double, double> >, std::vector <std::pair <double, double> >);
double lenghtOfVector(std::vector <std::pair <double, double> >);
double heightOfTriangle(std::pair <double, double>, std::pair <double, double>, std::pair <double, double>);
std::pair <double, double> findMinCoordDown(std::vector < std::pair <double, double> >, int);
std::pair <double, double> makeLine(std::pair <double, double>, std::pair <double, double>);
std::pair <double, double> upLine(std::pair <double, double>, std::pair <double, double>);
std::vector <std::pair <double, double> > moveVector(std::vector <std::pair <double, double> >, std::pair <double, double>);
std::vector <double> solveQU(double, double, double);
double findSquareOfTriangle(std::pair <double, double>, std::pair <double, double>, std::pair <double, double>);

#endif
