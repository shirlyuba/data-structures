#include "metric.h"

double getDistance(std::pair<double, double> coord1, std::pair<double, double> coord2)
{
	double dist = (coord2.first - coord1.first)*(coord2.first - coord1.first) + (coord2.second - coord1.second)*(coord2.second - coord1.second);
	return sqrt(dist);
};

std::pair<double, double> getCoordOfVector(std::vector <std::pair<double, double> > v)
{
	double x = v[1].first - v[0].first;
	double y = v[1].second - v[0].second;
	return{ x, y };
}

double det(std::vector<std::pair <double, double> > v1, std::vector <std::pair <double, double> > v2)
{
	std::pair<double, double> W1 = getCoordOfVector(v1);
	std::pair<double, double> W2 = getCoordOfVector(v2);
	return W1.first*W2.second - W1.second*W2.first;
};

double lenghtOfVector(std::vector <std::pair <double, double> > v)
{
	auto w = getCoordOfVector(v);
	return sqrt(w.first*w.first + w.second*w.second);
}
std::vector <std::pair <double, double> > createVector(std::pair <double, double> coord1, std::pair <double, double> coord2)
{
	std::vector <std::pair <double, double> > A;
	A.push_back(coord1);
	A.push_back(coord2);
	return A;
};


double myCos(std::vector <std::pair <double, double> > v1, std::vector <std::pair <double, double> > v2)
{
	return scalMultiple(v1, v2) / (lenghtOfVector(v1)*lenghtOfVector(v2));
};


double scalMultiple(std::vector <std::pair <double, double> > v1, std::vector <std::pair <double, double> > v2)
{
	double result;
	auto W1 = getCoordOfVector(v1);
	auto W2 = getCoordOfVector(v2);
	result = W1.first*W2.first + W1.second*W2.second;
	return result;
};

double heightOfTriangle(std::pair<double, double> t1, std::pair<double, double> t2, std::pair<double, double> t3)
{
	auto V1 = createVector(t1, t2);
	auto V2 = createVector(t1, t3);
	auto V3 = createVector(t2, t3);
	return fabs(det(V1, V2) / (lenghtOfVector(V3)));
};

std::pair<double, double> findMinCoordDown(std::vector < std::pair<double, double> > shape, int countOfAngle)
{
	auto minX = DBL_MAX;
	auto minY = DBL_MAX;
	for (int i = 0; i < countOfAngle - 1; i++)
	{
		if (shape[i].second < minY)
		{
			minY = shape[i].second;
			minX = shape[i].first;
		}
		if (shape[i].second == minY)
		{
			if (shape[i].first > minX)
			{
				minY = shape[i].second;
				minX = shape[i].first;
			}
		}
	}
	return{ minX, minY };
};

std::pair<double, double> makeLine(std::pair<double, double> p1, std::pair<double, double> p2)
{
	int sign;
	if (p1.second < p2.second)
	{
		std::vector<std::pair<double, double> > V1 = createVector(p1, p2);
		if (myCos(V1, createVector(p1, { INT_MAX, p1.second })) > 0)
			sign = 1;
		else
		{
			if (myCos(V1, createVector(p1, { INT_MAX, p1.second })) == 0)
				sign = 0;
			else sign = -1;
		}
	}
	else
	{
		std::vector<std::pair<double, double> > V1 = createVector(p2, p1);
		if (myCos(V1, createVector(p2, { INT_MAX, p2.second })) > 0)
			sign = 1;
		else
		{
			if (myCos(V1, createVector(p2, { INT_MAX, p2.second })) == 0)
				sign = 0;
			else sign = -1;
		}
	}
	double k;
	k = fabs((p2.second - p1.second) / (p2.first - p1.first));
	k = k * sign;
	double b;
	b = p1.second - k * p1.first;
	return{ k, b };
};

std::pair <double, double> upLine(std::pair <double, double> l1, std::pair <double, double> pointOfSimm)
{
	std::pair<double, double> l2;
	if (pointOfSimm.first > 0)
	{
		l2.second = l1.second - 0.01;
	}
	else l2.second = l1.second + 0.01;
	l2.first = (l1.second - pointOfSimm.second) / (l1.first);
	return l2;
};

std::vector <std::pair<double, double>> moveVector(std::vector <std::pair<double, double>> v1, std::pair<double, double> point)
{
	double delX = fabs(v1[1].first - v1[0].first);
	double delY = fabs(v1[1].second - v1[0].second);
	v1[0].first = point.first;
	v1[0].second = point.second;
	v1[1].first += delX;
	v1[1].second += delY;
	return createVector(v1[0], v1[1]);
};

std::vector <double> solveQU(double a, double b, double c)
{
	std::vector <double> solutions;
	double D = b * b - 4 * a * c;
	if (D > 0)
	{
		double x1 = (-b + sqrt(D)) / (2 * a);
		double x2 = (-b - sqrt(D)) / (2 * a);
		solutions.push_back(x1);
		solutions.push_back(x2);
	}
	if (D == 0)
	{
		double x = -b / (2 * a);
		solutions.push_back(x);
	}
	return solutions;
};

double findSquareOfTriangle(std::pair <double, double> p1, std::pair <double, double> p2, std::pair <double, double> p3)
{
	return det(createVector(p1, p2), createVector(p1, p3)) / 2;
}
