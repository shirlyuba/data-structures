#include<iostream>
#include<string>

using namespace std;

class Human {
public:
	Human(const string& first, const string& second)
	{
		name = first;
		surname = second;
	}
	string getName() {
		return name;
	};
	string getSurname() {
		return surname;
	};
	virtual void getTitle() {
		cout << getName() << " " << getSurname() << endl;
	};
	~Human() {};
private:
	string name;
	string surname;
};

class Miss: public Human {
public:
	Miss(string name, string surname) : Human(name, surname) {};
	void getTitle() override {
		cout << "Miss " << getSurname() << endl;
	}
	~Miss() {};
};

class Mister: public Human {
public:
	Mister(string name, string surname) : Human(name, surname) {};
	void getTitle() override {
		cout << "Mister " << getSurname() << endl;
	}
	~Mister() {};
};

class Child: public Human {
public:
	Child(string name, string surname) : Human(name, surname) {};
	void getTitle() override {
		cout << "child " << getName() << endl;
	}
	~Child() {};
};


int main()
{
	Human* human = new Human("Blake", "Marin");
	Human* man = new Mister("Dan", "Conrad");
	Human* women = new Miss("Lily", "Shay");
	Human* kid = new Child("Denis", "Rail");
	human->getTitle();
	man->getTitle();
	women->getTitle();
	kid->getTitle();
	delete human;
	delete man;
	delete women;
	delete kid;
	return 0;
}
