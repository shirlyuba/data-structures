#include<algorithm>
#include<iostream>
#include<string>
#include<vector>

using namespace std;

class Human
{
public:
	Human(const string& first, const string& second)
	{
		name = first;
		surname = second;
	};
	string getName() {
		return name;
	};
	string getSurname() {
		return surname;
	};
	virtual ~Human() {};
private:
	string name;
	string surname;
};

class worker : public Human
{
public:
	worker(string name, string surname) : Human(name, surname) {};
};

class student : public Human
{
public:
	student(string name, string surname) : Human(name, surname) {};
};

class buildings
{
public:
	buildings() {};
	virtual bool permission(const Human* h) 
	{
		return true;
	};
	virtual string getName()
	{
		return "this building";
	};
private:
	vector <const Human*> accepted;
	const Human* h;
};

class lab : public buildings
{
public:
	lab() {};
	bool permission(const Human* h)
	{
		const worker* et = dynamic_cast <const worker*> (h);
		if (et != NULL)
		{
			return true;
		}
		else
		{
			const student* id = dynamic_cast <const student*> (h);
			if (id != NULL)
			{
				if (find(accepted.begin(), accepted.end(), h) != accepted.end())
					return true;
				else accepted.push_back(h);
			}
		}
		return false;
	};
	string getName()
	{
		return "laboratory";
	};
private:
	vector <const Human*> accepted;
};

class university : public buildings
{
public:
	university() {};
	bool permission(const Human* h)
	{
		const student* id = dynamic_cast <const student*> (h);
		const worker* et = dynamic_cast <const worker*> (h);
		if ((id == NULL) && (et == NULL))
		{
			return false;
		}
		else
		{
			return true;
		}
	};
	string getName()
	{
		return "university";
	};
private:
};

void getAcces(Human* somebody, buildings* build)
{
	if (build->permission(somebody))
	{
		cout << somebody->getName() << " " << somebody->getSurname() << " can enter in the " << build->getName() << endl;
	}
	else
	{
		cout << somebody->getName() << " " << somebody->getSurname() << " can NOT enter in the " << build->getName() << endl;
	}
}

int main()
{
	Human* student1 = new student("Dan", "Conrad");
	Human* student2 = new student("Mary", "Rose");
	Human* professor = new worker("Alexey", "Crowd");
	Human* somebody = new Human("Denis", "Mitchel");
	buildings* lab1 = new lab();
	buildings* lab2 = new lab();
	buildings* univer = new university();
	buildings* build = new buildings();
	getAcces(student1, lab1);
	getAcces(student1, lab1);
	getAcces(student1, univer);
	getAcces(student1, build);
	getAcces(student1, lab2);
	getAcces(student1, lab2);
	getAcces(student2, univer);
	getAcces(student2, lab1);
	getAcces(student2, lab2);
	getAcces(student2, lab1);
	getAcces(student2, lab2);
	getAcces(student2, build);
	getAcces(professor, univer);
	getAcces(professor, lab1);
	getAcces(professor, lab1);
	getAcces(professor, lab2);
	getAcces(professor, build);
	getAcces(somebody, univer);
	getAcces(somebody, lab1);
	getAcces(somebody, lab1);
	getAcces(somebody, lab2);
	getAcces(somebody, lab2);
	getAcces(somebody, build);
	delete student1;
	delete student2;
	delete professor;
	delete somebody;
	delete lab1;
	delete lab2;
	delete univer;
	delete build;
	return 0;
}