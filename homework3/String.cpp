#include "classString.h"

int main()
{
	std::cout << "Enter two strings!\n";
	Str s;
	std::cin >> s;
	Str r;
	std::cin >> r;
	std::cout << s << std::endl << r << std::endl;
	s.append(r);
	std::cout << s << std::endl;
	s.insert(r, 4);
	std::cout << s << std::endl;
	s.erase(2, 6);
	s.deleteLast();
	std::cout << s;
	std::cin.get();
	std::cin.get();
	return 0;
}