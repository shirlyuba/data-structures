#ifndef LONGARIFMETIC_H

#define LONGARIFMETIC_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

#define BASE 10
#define DIMOFBASE 2

class BigInt
{
public:
  BigInt();
  BigInt(const BigInt & bint);
  BigInt(int num);
  BigInt(long long num);
  BigInt(std::string str);
	friend std::ostream & operator<< (std::ostream & os, const BigInt & bint);
	friend std::istream & operator>> (std::istream & is, BigInt & bint);

  void operator=(const BigInt & bint);
  BigInt operator-() const;
  BigInt operator+ (const BigInt & bint) const;
  friend BigInt operator-(const BigInt & a, const BigInt & b);
  friend BigInt operator/(const BigInt & a, const BigInt & b);
  friend BigInt operator%(const BigInt & a, const BigInt & b);
  friend BigInt operator*(const BigInt & a, const BigInt & b);

  friend bool operator> (const BigInt& a, const BigInt& b);
  friend bool operator>= (const BigInt& a, const BigInt& b);
  friend bool operator< (const BigInt& a, const BigInt& b);
  friend bool operator<= (const BigInt& a, const BigInt& b);
  friend bool operator== (const BigInt& a, const BigInt& b);
  friend bool operator!= (const BigInt& a, const BigInt& b);

private:
  std::vector<char> _arr;
  int _size;
  char _sign;
};

#endif