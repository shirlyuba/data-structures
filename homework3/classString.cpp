#include "classString.h"

Str::Str(char* s)
{
	len_ = strlen(s);
	arr_ = new char[len_ + 1];
	capacity_ = len_ + 1;
	for (size_t i = 0; i < len_; ++i)
		arr_[i] = s[i];
}

Str::Str(std::string & s)
{
	len_ = s.length();
	arr_ = new char[len_ + 1];
	capacity_ = len_ + 1;
	for (size_t i = 0; i < len_; ++i)
		arr_[i] = s[i];
	arr_[len_] = '\0';
}

Str::Str(const Str & s) : capacity_(s.capacity_), len_(s.len_)
{
	arr_ = new char[capacity_];
	strcpy(arr_, s.arr_);
}

std::ostream & operator<<(std::ostream & os, Str & s)
{
	for (size_t i = 0; i < s.len_; ++i)
		std::cout << s.arr_[i];
	return os;
}

void Str::operator=(const Str & s)
{
	len_ = s.len_;
	capacity_ = s.capacity_;
	arr_ = new char[capacity_];
	strcpy(arr_, s.arr_);
}

std::istream & operator>> (std::istream & is, Str & s)
{
	std::string string;
	std::cin >> string;
	s = Str(string);
	return is;
}

bool operator<(Str & s1, Str & s2)
{
	if (strcmp(s1.arr_, s2.arr_) == -1)
		return 1;
	return 0;
}

bool operator>(Str & s1, Str & s2)
{
	if (strcmp(s1.arr_, s2.arr_) == 1)
		return 1;
	return 0;
}

bool operator==(Str & s1, Str & s2)
{
	if (strcmp(s1.arr_, s2.arr_) == 0)
		return 1;
	return 0;
}

bool operator!=(Str & s1, Str & s2)
{
	if (strcmp(s1.arr_, s2.arr_) != 0)
		return 1;
	return 0;
}

char* Str::operator[] (int index)
{
	return arr_ + index;
}

void Str::insert(Str s, int index)
{
	char* new_arr;
	while (len_ + s.len_ >= capacity_)
		capacity_ *= 2;
	new_arr = new char[capacity_];
	strcpy(new_arr, arr_);
	char* tmp = arr_;
	arr_ = new_arr;
	delete[] tmp;
	for (unsigned int i = 0; i < s.len_; ++i) {
		arr_[index + s.len_ + i] = arr_[index + i];
		arr_[index + i] = s.arr_[i];
	}
	len_ += s.len_;
	arr_[len_] = '\0';
}

void Str::append(const Str & s)
{
	char* new_arr;
	while (len_ + s.len_ >= capacity_)
		capacity_ *= 2;
	new_arr = new char[capacity_];
	strcpy(new_arr, arr_);
	char* tmp = arr_;
	arr_ = new_arr;
	delete[] tmp;
	for (unsigned int i = 0; i < s.len_; ++i) {
		arr_[len_ + i] = s.arr_[i];
	}
	len_ += s.len_;
	arr_[len_] = '\0';
}

void Str::erase(int index, int num)
{
	for (int i = 0; i < num; ++i)
		arr_[index + i] = arr_[index + i + num];
	len_ -= num;
	if (len_ < capacity_ / 3) {
		char* new_arr = new char[capacity_ / 2 + 1];
		strcpy(new_arr, arr_);
		capacity_ = capacity_ / 2 + 1;
		char * tmp = arr_;
		arr_ = new_arr;
		delete[] tmp;
	}
}

void Str::deleteLast()
{
	arr_[len_ - 1] = '\0';
	--len_;
	if (len_ < capacity_ / 3) {
		char* new_arr = new char[capacity_ / 2 + 1];
		strcpy(new_arr, arr_);
		capacity_ = capacity_ / 2 + 1;
		char * tmp = arr_;
		arr_ = new_arr;
		delete[] tmp;
	}
}
