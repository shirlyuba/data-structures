#include"metric.h"

using namespace std;

class shapes
{
public:
	shapes(int count)
	{
		createShape(count);
	}
	void createShape(int countOfAngle)
	{
		for (int i = 0; i < countOfAngle; i++)
		{
			cout << " enter the " << i + 1 << " coordinates: " << endl;
			addCoordinates();
		}
	}
	void addCoordinates()
	{
		double x;
		double y;
		cin >> x >> y;
		coordinates.push_back({ x,y });
	}
	void printPointsOfIntersect(vector <pair <double, double> > segment)
	{
		auto points = findIntersect(segment);
		cout << "the points of intersect: " << endl;
		if (points.size() != 0)
		{
			for (int i = 0; i < points.size(); i++)
			{
				cout << points[i].first << ", " << points[i].second << endl;
			}
		}
		else
		{
			cout << "dont exist" << endl;
		}
	}
	virtual double findSquare()
	{
		return 0;
	};
	virtual bool checkOnBoarder()
	{
		return false;
	};
	virtual bool checkAttach()
	{
		return false;
	};
	vector <pair <double, double> > findIntersect(vector <pair <double, double> > segment)
	{
		int count = coordinates.size();
		vector <pair <double, double> > pointsOfIntersect;
		if (segment[0].first == segment[1].first)
		{
			for (int i = 0; i < count - 1; i++)
			{
				auto st = createVector(coordinates[i], coordinates[i + 1]);
				if (checkIntersect(segment, st))
				{
					double x = segment[0].first;
					double y = x * makeLine(segment[0], segment[1]).first + makeLine(segment[0], segment[1]).second;
					pointsOfIntersect.push_back(make_pair(x, y));
				}
			}
			if (checkIntersect(segment, createVector(coordinates[count - 1], coordinates[0])))
			{
				double x = segment[0].first;
				double y = x * makeLine(segment[0], segment[1]).first + makeLine(segment[0], segment[1]).second;
				pointsOfIntersect.push_back(make_pair(x, y));
			}
		}
		else
		{
			for (int i = 0; i < count - 1; i++)
			{
				auto st = createVector(coordinates[i], coordinates[i + 1]);
				if (checkIntersect(segment, st))
				{
					double x;
					double y;
					if (st[0].first == st[1].first)
					{
						x = coordinates[i].first;
						y = x * makeLine(segment[0], segment[1]).first + makeLine(segment[0], segment[1]).second;
						pointsOfIntersect.push_back(make_pair(x, y));
					}
					else
					{
						x = ((makeLine(st[0], st[1]).second - makeLine(segment[0], segment[1]).second) / ((makeLine(segment[0], segment[1]).first - makeLine(st[0], st[1]).first)));
						y = x * makeLine(st[0], st[1]).first + makeLine(st[0], st[1]).second;
						pointsOfIntersect.push_back(make_pair(x, y));
					}
				}
			}
			auto st = createVector(coordinates[count - 1], coordinates[0]);
			if (checkIntersect(segment, st))
			{
				double x;
				double y;
				if (st[0].first == st[1].first)
				{
					x = st[0].first;
					y = x * makeLine(segment[0], segment[1]).first + makeLine(segment[0], segment[1]).second;
					pointsOfIntersect.push_back(make_pair(x, y));
				}
				else
				{
					x = ((makeLine(st[0], st[1]).second - makeLine(segment[0], segment[1]).second) / ((makeLine(segment[0], segment[1]).first - makeLine(st[0], st[1]).first)));
					y = x * makeLine(st[0], st[1]).first + makeLine(st[0], st[1]).second;
					pointsOfIntersect.push_back(make_pair(x, y));
				}
			}
		}
		return pointsOfIntersect;
	}
	bool checkIntersect(vector <pair <double, double> > segment, vector <pair <double, double> > st)
	{
		if ((det(st, createVector(st[0], segment[0])) * (det(st, createVector(st[0], segment[1]))) <= 0) &&
			(det(segment, createVector(segment[0], st[0])) * det(segment, createVector(segment[0], st[1])) <= 0))
			return true;
		else return false;
	}
protected:
	vector <pair<double,double>> coordinates;
};

class Circle : public shapes
{
public:
	Circle() : shapes(2) {}; /* THE MIDDLE AND ONE POINT FROM THE CIRCLE*/
	double findSquare()
	{
		double sqrR = pow(getDistance(coordinates[0], coordinates[1]), 2);
		return sqrR*M_PI;
	}
	bool checkOnBoarder(pair<double, double> coord1)
	{
		double R = getDistance(coordinates[0], coordinates[1]);
		if (getDistance(coordinates[0], coord1) == R)
		{
			return true;
		}
		else return false;
	};
	bool checkAttach(pair<double, double> coord1)
	{
		double R = getDistance(coordinates[0], coordinates[1]);
		if (pow((coord1.first - coordinates[0].first),2) + pow((coord1.second - coordinates[0].second), 2) <= R)
		{
			return true;
		}
		else return false;
	}
	void printPointsOfIntersect(vector <pair <double, double> > segment)
	{
		auto points = findIntersect(segment);
		cout << "the points of intersection" << endl;
		if (points.size() != 0)
		{
			for (int i = 0; i < points.size(); i++)
			{
				cout << points[i].first << ", " << points[i].second << endl;
			}
		}
		else
		{
			cout << "dont exist" << endl;
		}
	}
private:
	bool checkAttachToSegment(vector <pair <double, double> > segment, pair < double, double> point)
	{
		if (((segment[0].second <= point.second) && (point.second <= segment[1].second)) ||
			((segment[0].second >= point.second) && (point.second >= segment[1].second)) &&
			(((segment[0].first <= point.first) &&  (point.first <= segment[1].first)) ||
				((segment[0].first >= point.first) && (point.first >= segment[1].first))))
		{
			return true;
		}
		return false;
	}
	vector <pair <double, double> > findIntersect(vector <pair<double, double>> segment)
	{
		double R = getDistance(coordinates[0], coordinates[1]);
		vector <pair <double, double> > points;
		if (segment[0].first == segment[1].first)
		{
			double a = 1;
			double b = -2 * coordinates[0].second;
			double c = pow(coordinates[0].first, 2) - 2 * coordinates[0].first * segment[0].first + pow(segment[0].first, 2) + pow(coordinates[0].second, 2) - R * R;
			auto Y = solveQU(a, b, c);
			for (int i = 0; i < Y.size(); i++)
			{
				if (checkAttachToSegment(segment, { segment[0].first, Y[i] }))
				{
					points.push_back({ segment[0].first, Y[i] });
				}
			}
		}
		else
		{
			auto line = makeLine(segment[0], segment[1]);
			double a = 1 + pow(line.first, 2);
			double b = 2 * (line.first * (line.second - coordinates[0].second) - coordinates[0].first);
			double c = pow(coordinates[0].first, 2) + pow(line.second - coordinates[0].second, 2) - R * R;
			auto X = solveQU(a, b, c);
			vector <double> Y;
			for (int i = 0; i < X.size(); i++)
			{
				double y = line.first * X[i] + line.second;
				Y.push_back(y);
				if (checkAttachToSegment(segment, { X[i], Y[i] }))
				{
					points.push_back(make_pair(X[i], Y[i]));
				}
			}
		}
		return points;
	}
};



class Polygon : public shapes
{
public:
	Polygon(int count) : shapes(count)
	{
		while (checkPolygon())
		{
			coordinates.clear();
			createShape(count);
		}
	}
	bool checkAttach(pair <double, double> coord1)
	{
		auto points = findIntersect(createVector(coord1, { INT_MAX, coord1.second }));
		int count = points.size();
		for (auto it : points)
			for (int i = 0; i < coordinates.size(); i++)
			{
				if (it == coordinates[i])
				{
					count = count - 1;
				}
			}
		if (count % 2 == 1)
		{
			return true;
		}
		else return false;
	}
	double findSquare()
	{
		int count = coordinates.size();
		pair <double, double> min = findMinCoordDown(coordinates, count);
		double S = findSquareOfTriangle({ min.first, min.second - 10000 }, coordinates[count - 1], coordinates[0]);
		for (int i = 0; i <= count - 2; i++)
		{
			S += findSquareOfTriangle({ min.first, min.second - 10000 }, coordinates[i], coordinates[i + 1]);
		}
		return fabs(S);
	}
	bool checkOnBoarder(pair<double, double> coord1)
	{
		int count = coordinates.size();
		auto line = makeLine(coordinates[0], coordinates[count - 1]);
		if ((coord1.second == line.first * coord1.first + line.second) || (coordinates[0].first == coordinates[count - 1].first))
		{
			if (checkAttachToSegment(createVector(coordinates[0], coordinates[count - 1]), coord1))
			{
				return true;
			}
		}
		for (int i = 0; i < count - 1; i++)
		{
			auto line1 = makeLine(coordinates[i], coordinates[i + 1]);
			if ((coord1.second == line1.first * coord1.first + line1.second) || (coordinates[i].first == coordinates[i + 1].first))
			{
				if (checkAttachToSegment(createVector(coordinates[i], coordinates[i + 1]), coord1))
				{
					return true;
				}
			}
		}
		return false;
	}
private:
	bool checkAttachToSegment(vector <pair <double, double> > segment, pair < double, double> point)
	{
		if ((((segment[0].second <= point.second) && (point.second <= segment[1].second)) ||
			((segment[0].second >= point.second) && (point.second >= segment[1].second))) &&
			(((segment[0].first <= point.first) && (point.first <= segment[1].first)) ||
				((segment[0].first >= point.first) && (point.first >= segment[1].first))))
		{
			return true;
		}
		return false;
	}
	int checkPolygon()
	{
		for (int i = 0; i < coordinates.size() - 2; i++)
		{
			if (det(createVector(coordinates[i], coordinates[i + 1]), createVector(coordinates[i + 1], coordinates[i + 2])) >= 0)
			{
				return 0;
			}
		}
		if (det(createVector(coordinates[coordinates.size() - 1], coordinates[0]), createVector(coordinates[0], coordinates[1])) >= 0)
		{
			return 0;
		}
		cout << "it is not Polygon" << endl;
		return 1;
	}
};


class convexPolygon : public shapes
{
public:
	convexPolygon(int count) : shapes(count)
	{
		while (checkPolygon())
		{
			coordinates.clear();
			createShape(count);
		}
	};
	bool checkAttach(pair<double, double> coord1)
	{
		int count = coordinates.size();
		if (det(createVector(coordinates[count - 1], coordinates[0]), createVector(coordinates[count - 1], coord1)) < 0)
			return false;
		for (int i = 0; i < count - 1; i++)
		{
			if (det(createVector(coordinates[i], coordinates[i + 1]), createVector(coordinates[i], coord1)) < 0)
				return false;
		}
		return true;
	}
	bool checkOnBoarder(pair <double, double> coord1)
	{
		int count = coordinates.size();
		for (int i = 0; i < count; i++)
		{
			if (coord1 == coordinates[i])
				return true;
		}
		for (int i = 0; i < count - 1; i++)
		{
			if ((coordinates[i].first == coord1.first) && (coordinates[i].first == coordinates[i + 1].first))
			{
				if ((coord1.second - coordinates[i].second) * (coord1.second - coordinates[i + 1].second) < 0)
				{
					return true;
				}
				else return false;
			}
			if ((coordinates[i].second == coord1.second) && (coordinates[i].second == coordinates[i + 1].second))
			{
				if ((coord1.first - coordinates[i].first) * (coord1.first - coordinates[i + 1].first) < 0)
				{
					return true;
				}
				else return false;
			}
			if (((coordinates[i].first - coord1.first) / (coordinates[i].second - coord1.second)) == ((coordinates[i + 1].first - coord1.first) / (coordinates[i + 1].second - coord1.second)))
				return true;
		}
		if (coordinates[0].first == coord1.first)
		{
			if ((coord1.second - coordinates[0].second) * (coord1.second - coordinates[count - 1].second) < 0)
			{
				return true;
			}
			else return false;
		}
		if (coordinates[0].second == coord1.second)
		{
			if ((coord1.first - coordinates[0].first) * (coord1.first - coordinates[count - 1].first) < 0)
			{
				return true;
			}
			else return false;
		}
		if (((coordinates[count - 1].first - coord1.first) / (coordinates[count - 1].second - coord1.second)) == ((coordinates[0].first - coord1.first) / (coordinates[0].second - coord1.second)))
			return true;
		return false;
	}
	double findSquare()
	{
		int count = coordinates.size();
		double S = 0;
		for (int i = 1; i <= count - 2; i++)
		{
			S += findSquareOfTriangle(coordinates[0], coordinates[i], coordinates[i + 1]);
		}
		return S;
	}
private:
	int checkPolygon()
	{
		for (int i = 0; i < coordinates.size() - 2; i++)
		{
			if (det(createVector(coordinates[i], coordinates[i + 1]), createVector(coordinates[i + 1], coordinates[i + 2])) < 0)
			{
				cout << "it is not convex Polygon" << endl;
				return 1;
			}
		}
		if (det(createVector(coordinates[coordinates.size() - 1], coordinates[0]), createVector(coordinates[0], coordinates[1])) < 0)
		{
			cout << "it is not convex Polygon" << endl;
			return 1;
		}
		return 0;
	}
};

class Rectangle : public convexPolygon
{
public:
	Rectangle(int count) : convexPolygon(count)
	{
		while (checkRectangle())
		{
			coordinates.clear();
			createShape(4);
		}
	}
private:
	int checkRectangle()
	{
		for (int i = 0; i < coordinates.size() - 2; i++)
		{
			if (myCos(createVector(coordinates[i], coordinates[i + 1]), createVector(coordinates[i + 1], coordinates[i + 2])) != 0)
			{
				cout << "it is not rectangle" << endl;
				return 1;
			}
		}
		if (myCos(createVector(coordinates[2], coordinates[3]), createVector(coordinates[3], coordinates[0])) != 0)
		{
			cout << "it is not rectangle" << endl;
			return 1;
		}
		return 0;
	}
};


class Square : public convexPolygon {
public:
	Square(int count) : convexPolygon(count)
	{
		while (checkSquare())
		{
			coordinates.clear();
			createShape(count);
		}
	}
private:
	int checkSquare()
	{
		double len = getDistance(coordinates[3], coordinates[0]);
		for (int i = 0; i < coordinates.size() - 1; i++)
		{
			if (len != getDistance(coordinates[i], coordinates[i + 1]))
			{
				cout << "it is not square" << endl;
				return 1;
			}
		}
		if (myCos(createVector(coordinates[0], coordinates[1]), createVector(coordinates[1], coordinates[2])) != 0)
		{
			cout << "it is not square" << endl;
			return 1;
		}
		return 0;
	}
};

class Triangle : public convexPolygon
{
public:
	Triangle(int count) : convexPolygon(count) {};
};

int main()
{
	cout << "you should enter the coordinates against clockwise" << endl;
	cout << "______________CIRCLE______________" << endl;
	Circle* A = new Circle();
	cout << "the square is " << A->findSquare() << endl;
	if (A->checkOnBoarder({ 3,0 }))
		cout << "yes" << endl;
	auto V1 = createVector({ 4,4 }, { 7,7 });
	auto V2 = createVector({ -1,-5 }, { 1,5 });
	auto V3 = createVector({ 0, -5 }, { 0,5});
	A->printPointsOfIntersect(V1);
	cout << "___" << endl;
	A->printPointsOfIntersect(V2);
	cout << "___" << endl;
	A->printPointsOfIntersect(V3);
	cout << "_______________SQUARE______________" << endl;
	convexPolygon* B = new Square(4);
	cout << "the square is: " << B->findSquare() << endl;
	if (B->checkAttach({ 1, 0 }))
		cout << "yes1" << endl;
	else cout << "no1" << endl;
	if (B->checkAttach({ 3,0 }))
		cout << "yes2" << endl;
	else cout << "no2" << endl;
	if (B->checkOnBoarder({ 3,3 }))
		cout << "yes3" << endl;
	else cout << "no3" << endl;
	if (B->checkOnBoarder({ 3,5 }))
		cout << "yes4" << endl;
	else cout << "no4" << endl;
	if (B->checkOnBoarder({ 5,5 }))
		cout << "yes5" << endl;
	else cout << "no5" << endl;
	auto V4 = createVector({ 2,2 }, { 4,4 });
	auto V5 = createVector({ 0,-1 }, { -1,3 });
	B->printPointsOfIntersect(V4);
	B->printPointsOfIntersect(V5);
	cout << "__________________TRIANGLE___________________" << endl;
	convexPolygon* C = new Triangle(3);
	cout << "the square is: " << C->findSquare() << endl;
	if (C->checkOnBoarder({ 0, 0 }))
	    cout << "yes1" << endl;
	else cout << "no1" << endl;
	if (C->checkOnBoarder({ 9, 0 }))
		cout << "yes2" << endl;
	else cout << "no2" << endl;
	cout << "_____________________RECTANGLE__________________" << endl;
	convexPolygon* D = new Rectangle(4);
	cout << "the square is: " << D->findSquare() << endl;
	cout << "_____________________POLYGON_______________" << endl;
	Polygon* E = new Polygon(4);
	cout << "the square is: " << E->findSquare() << endl;
	if (E->checkAttach({ 3,1}))
	{
		cout << "yes1" << endl;
	}
	else cout << "no1" << endl;
	if (E->checkOnBoarder({ 0,3 }))
	{
		cout << "yes2" << endl;
	}
	else cout << "no2" << endl;
	if (E->checkOnBoarder({ 5,0 }))
	{
		cout << "yes3" << endl;
	}
	else cout << "no3" << endl;
	auto V6 = createVector({ 0, -1 }, { 6, -1 });
	E->printPointsOfIntersect(V6);
	delete A;
	delete B;
	delete C;
	delete D;
	delete E;
	return 0;
}
