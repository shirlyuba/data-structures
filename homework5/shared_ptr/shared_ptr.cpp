#include <iostream>

template <typename T>
class Helper
{
public:
	T* pointer;
	int* count;
	Helper() : pointer(nullptr), count(nullptr) {};
	Helper(T* new_pointer) : pointer(new_pointer), count(new int(1)) {};
	Helper(const Helper <T> & helper) : pointer(helper.pointer), count(helper.count) {};
	Helper(Helper <T> && helper) : pointer(helper.pointer), count(helper.count) { helper = Helper <T>(); };
	Helper& operator=(const Helper <T> & helper)
	{
		pointer = helper.pointer;
		count = helper.count;
		return *this;
	}
	Helper& operator=(Helper <T> && helper)
	{
		std::swap(pointer, helper.pointer);
		std::swap(count, helper.count);
		return *this;
	}
	operator bool() const { return pointer != nullptr; }
};

template <typename T>
class SharedPtr
{
private:
	Helper <T> helper;
	void decrement()
	{
		if (helper)
			if (--(*(helper.count)) == 0)
			{
				helper.pointer = nullptr;
				helper = Helper <T>();
			}
	}
public:
	SharedPtr() : helper(Helper <T>()) {}
	SharedPtr(T* pointer) : helper(Helper <T>(pointer)) {}
	SharedPtr(Helper <T> new_helper) : helper(new_helper) {}
	SharedPtr(const SharedPtr <T> & shared_pointer) : helper(shared_pointer.helper) { ++(*(helper.count)); }
	SharedPtr(SharedPtr <T> && shared_pointer) : helper(shared_pointer.helper)
	{
		shared_pointer.helper.pointer = nullptr;
		shared_pointer.helper.count = nullptr;
	}
	SharedPtr& operator=(const SharedPtr <T> & shared_pointer)
	{
		if (helper.pointer != shared_pointer.helper.pointer) decrement();
		helper = shared_pointer.helper;
		++(*(helper.count));
		return *this;
	}
	SharedPtr& operator=(SharedPtr <T> && shared_pointer)
	{
		swap(helper.ptr, shared_pointer.helper.ptr);
		swap(helper.count, shared_pointer.helper.count);
		return *this;
	}
	int getCount() const { if (helper) return *(helper.count); }
	T* get() const { if (helper) return helper.pointer; }
	T& operator*() const { if (helper) return *helper.pointer; }
	T* operator->() const { if (helper) return helper.pointer; }
	~SharedPtr() { decrement(); }
};

int main()
{
	int* a = new int(123);
	SharedPtr <int> shared_pointer1(a);
	std::cout << shared_pointer1.get() << std::endl;
	std::cout << *shared_pointer1 << std::endl;
	std::cout << shared_pointer1.getCount() << std::endl;
	std::cout << std::endl;
	SharedPtr <int> shared_pointer2(shared_pointer1);
	std::cout << shared_pointer2.get() << std::endl;
	std::cout << *shared_pointer2 << std::endl;
	std::cout << shared_pointer2.getCount() << std::endl;
	std::cout << shared_pointer1.getCount() << std::endl;
	std::cout << std::endl;
	SharedPtr <int> shared_pointer3;
	std::cout << shared_pointer3.getCount() << std::endl;
	shared_pointer3 = shared_pointer2;
	std::cout << shared_pointer3.get() << std::endl;
	std::cout << *shared_pointer3 << std::endl;
	std::cout << shared_pointer3.getCount() << std::endl;
	std::cout << shared_pointer2.getCount() << std::endl;
	std::cout << shared_pointer1.getCount() << std::endl;
	std::cout << std::endl;
	int b = 124;
	SharedPtr <int> shared_pointer4(&b);
	std::cout << shared_pointer4.get() << std::endl;
	std::cout << *shared_pointer4 << std::endl;
	std::cout << shared_pointer4.getCount() << std::endl;
	shared_pointer3 = shared_pointer4;
	shared_pointer2 = shared_pointer4;
	std::cout << shared_pointer1.getCount() << std::endl;
	std::cout << shared_pointer3.getCount() << std::endl;
	shared_pointer2.~SharedPtr();
	std::cout << shared_pointer3.getCount() << std::endl;
	shared_pointer3.~SharedPtr();
	std::cout << shared_pointer4.getCount() << std::endl;
	return 0;
}