#include <iostream> 
#include <memory> 
#include <cassert> 

using namespace std;

class BST
{
public:
	int find(int key, shared_ptr <BST> tree);
	void insert(int key, int value, shared_ptr <BST> & tree);
	void erase(int key, shared_ptr <BST> & mainTree);
	void print(shared_ptr <BST> tree);
	shared_ptr <BST> findNearestNode(int key, shared_ptr <BST> tree);
	int get_key() { return key; };
	int get_value() { return value; }
private:
	int key;
	int value;
	shared_ptr <BST> left;
	shared_ptr <BST> right;
};


int BST::find(int key, shared_ptr <BST> tree)
{
	while ((key != tree->key) && (tree != nullptr))
	{
		if (key < tree->key)
		{
			tree = tree->left;
		}
		else tree = tree->right;
	}
	assert(tree != nullptr);
	return tree->value;
}


void BST::insert(int key, int value, shared_ptr <BST> & tree)
{
	if (tree == nullptr)
	{
		tree = make_shared <BST>();
		tree->key = key;
		tree->value = value;
		tree->right = nullptr;
		tree->left = nullptr;
	}
	else
	{
		if (key < tree->key)
		{
			insert(key, value, tree->left);
		}
		if (key > tree->key)
		{
			insert(key, value, tree->right);
		}
	}
}


void BST::erase(int key, shared_ptr <BST> & mainTree)
{
	shared_ptr <BST> tree = mainTree;
	shared_ptr <BST> prev = nullptr;
	int side = -1;
	while ((key != tree->key) && (tree != nullptr))
	{
		prev = tree;
		if (key < tree->key)
		{
			side = 1;
			tree = tree->left;
		}
		else
		{
			side = 0;
			tree = tree->right;
		}
	}
	assert(tree != nullptr);
	if ((tree->left == nullptr) && (tree->right == nullptr))
	{
		if (side == 1)
			prev->left = nullptr;
		if (side == 0)
			prev->right = nullptr;
		tree.~shared_ptr();
		return;
	}
	if ((tree->right == nullptr) && (tree->left != nullptr))
	{
		if (prev == nullptr)
		{
			mainTree = tree->left;
			return;
		}
		else
		{
			if (side)
				prev->left = tree->left;
			else
				prev->right = tree->left;
			tree.~shared_ptr();
			return;
		}
	}
	if ((tree->left == nullptr) && (tree->right != nullptr))
	{
		if (prev == nullptr)
		{
			mainTree = tree->right;
			return;
		}
		else
		{
			if (side)
				prev->left = tree->right;
			else
				prev->right = tree->right;
			tree.~shared_ptr();
			return;
		}
	}
	if ((tree->left != nullptr) && (tree->right != nullptr))
	{
		shared_ptr <BST> node = tree->right;
		if (node->left == nullptr)
		{
			node->left = tree->left;
			if (prev == nullptr)
			{
				mainTree = node;
				tree.~shared_ptr();
			}
			else
			{
				if (side)
				{
					prev->left = node;
				}
				else
				{
					prev->right = node;
				}
				tree.~shared_ptr();
			}
		}
		else
		{
			shared_ptr <BST> prevNode = nullptr;
			while (node->left != nullptr)
			{
				prevNode = node;
				node = node->left;
			}
			tree->key = node->key;
			tree->value = node->value;
			if (node->right == nullptr)
			{
				prevNode->left = nullptr;
				node.~shared_ptr();
			}
			else
			{
				node = node->right;
				prevNode->left = node;
			}
		}
	}
}


void BST::print(shared_ptr <BST> tree)
{
	if (tree->left)
		print(tree->left);
	cout << tree->key << " ";
	if (tree->right)
		print(tree->right);
}


shared_ptr <BST> BST::findNearestNode(int key, shared_ptr <BST> tree)
{
	shared_ptr <BST> potencialNode = nullptr;
	while (tree != nullptr)
	{
		if (tree->key == key)
		{
			return tree;
		}
		if (tree->key > key)
		{
			potencialNode = tree;
			if (tree->left != nullptr)
				tree = tree->left;
			else
				return potencialNode;
		}
		if (tree->key < key)
		{
			if (tree->right != nullptr)
				tree = tree->right;
			else return potencialNode;
		}
	}
	return potencialNode;
}


void checkNearestNode(shared_ptr <BST> nearestNode)
{
	if (nearestNode == nullptr)
		cout << "such a key doesn't exist" << endl;
	else
		cout << nearestNode->get_key() << " " << nearestNode->get_value() << endl;
}


int main()
{
	shared_ptr <BST> tree = nullptr;
	tree->insert(0, 1, tree);
	tree->insert(-5, 4, tree);
	tree->insert(3, 4, tree);
	tree->insert(-7, 4, tree);
	tree->insert(-3, 5, tree);
	tree->insert(1, 5, tree);
	tree->insert(5, 5, tree);
	tree->insert(-4, 5, tree);
	tree->insert(-1, 5, tree);
	tree->insert(4, 5, tree);
	tree->insert(16, 5, tree);
	tree->insert(10, 5, tree);
	tree->insert(12, 5, tree);
	tree->insert(9, 5, tree);
	tree->insert(7, 5, tree);
	tree->print(tree);
	cout << endl;
	shared_ptr <BST> nearestNode1 = tree->findNearestNode(-2, tree);
	checkNearestNode(nearestNode1);
	shared_ptr <BST> nearestNode2 = tree->findNearestNode(-150, tree);
	checkNearestNode(nearestNode2);
	shared_ptr <BST> nearestNode3 = tree->findNearestNode(150, tree);
	checkNearestNode(nearestNode3);
	shared_ptr <BST> nearestNode4 = tree->findNearestNode(12, tree);
	checkNearestNode(nearestNode4);
	shared_ptr <BST> nearestNode5 = tree->findNearestNode(3, tree);
	checkNearestNode(nearestNode5);
	shared_ptr <BST> nearestNode6 = tree->findNearestNode(4, tree);
	checkNearestNode(nearestNode6);
	shared_ptr <BST> nearestNode7 = tree->findNearestNode(6, tree);
	checkNearestNode(nearestNode7);
	shared_ptr <BST> nearestNode8 = tree->findNearestNode(-3, tree);
	checkNearestNode(nearestNode8);
	return 0;
}