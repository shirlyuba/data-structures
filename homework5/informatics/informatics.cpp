#include<iostream>
#include<vector>
#include<string>
#include<map>
#include<algorithm>
#include<memory>

using namespace std;

class olympiads
{
public:
	vector <double> getBals(int N);
	void SOLVED(map <string, vector <pair<int, double> > > & students, const vector <double> & bals);
	void STUD_STAT(const string & family, map <string, vector <pair<int, double> > > & students);
	void STUDS_BY_TASKS(const map <string, vector <pair<int, double> > > & students);
	void STUDS_BY_BALLS(const map <string, vector <pair<int, double> > > & students);
	void STUDS_MORE_TASKS(const map <string, vector <pair<int, double> > > & students);
	void STUDS_MORE_BALLS(const map <string, vector <pair<int, double> > > & students);
	void TASKS_BY_SOLUTIONS(const map <string, vector <pair<int, double> > > & students, int N);
	void solution(map <string, vector <pair<int, double> > > & students, int N, const vector <double> & bals);
};

vector <double> olympiads::getBals(int N)
{
	vector <double> bals;
	for (size_t i = 0; i < N; i++)
	{
		int x;
		cin >> x;
		bals.push_back(x);
	}
	return bals;
}

bool cmpTasks(const pair <string, vector <pair<int, double> > > & a, const pair <string, vector <pair<int, double> > > & b)
{
	return a.second.size() < b.second.size();
}

bool cmpBalls(const pair <string, vector <pair<int, double> > > & a, const pair <string, vector <pair<int, double> > > & b)
{
	double sumOfA = 0;
	double sumOfB = 0;
	for (int i = 0; i < a.second.size(); i++)
	{
		sumOfA += a.second[i].second;
	}
	for (int i = 0; i < b.second.size(); i++)
	{
		sumOfB += b.second[i].second;
	}
	return sumOfA < sumOfB;
}

void olympiads::SOLVED(map <string, vector <pair<int, double> > > & students, const vector <double> & bals)
{
	string family;
	int number;
	cin >> family >> number;
	students[family].push_back({ number, bals[number - 1] });
}

void olympiads::STUD_STAT(const string & family, map <string, vector <pair<int, double> > > & students)
{
	double sum = 0;
	for (int i = 0; i < students[family].size(); i++)
	{
		sum += students[family][i].second;
	}
	cout << students[family].size() << " " << sum << endl;
}

void olympiads::STUDS_BY_TASKS(const map <string, vector <pair<int, double> > > & students)
{
	vector < pair <string, vector < pair<int, double> > > > vec(students.begin(), students.end());
	sort(vec.begin(), vec.end(), cmpTasks);
	for (auto it : vec)
	{
		cout << it.first << " " << it.second.size() << endl;
	}
}

void olympiads::STUDS_BY_BALLS(const map <string, vector <pair<int, double> > > & students)
{
	vector < pair <string, vector < pair<int, double> > > > vec(students.begin(), students.end());
	sort(vec.begin(), vec.end(), cmpBalls);
	vector <double> balls;
	for (int i = 0; i < vec.size(); i++)
	{
		double sum = 0;
		for (int j = 0; j < vec[i].second.size(); j++)
		{
			sum += vec[i].second[j].second;
		}
		balls.push_back(sum);
	}
	int i = 0;
	for (auto it : vec)
	{
		cout << it.first << " " << balls[i] << endl;
		++i;
	}
}

void olympiads::STUDS_MORE_TASKS(const map <string, vector <pair<int, double> > > & students)
{
	int n;
	cin >> n;
	vector < pair <string, vector < pair<int, double> > > > vec(students.begin(), students.end());
	int i = 0;
	sort(vec.begin(), vec.end(), cmpTasks);
	while (vec[i].second.size() < n)
	{
		++i;
	}
	for (int j = i; j < vec.size(); j++)
	{
		cout << vec[j].first << " " << vec[j].second.size() << endl;
	}
}

void olympiads::STUDS_MORE_BALLS(const map <string, vector <pair<int, double> > > & students)
{
	int n;
	cin >> n;
	vector < pair <string, vector < pair<int, double> > > > vec(students.begin(), students.end());
	sort(vec.begin(), vec.end(), cmpBalls);
	vector <double> balls;
	for (int i = 0; i < vec.size(); i++)
	{
		double sum = 0;
		for (int j = 0; j < vec[i].second.size(); j++)
		{
			sum += vec[i].second[j].second;
		}
		balls.push_back(sum);
	}
	int i = 0;
	while (balls[i] < n)
	{
		i++;
	}
	for (int j = i; j < vec.size(); j++)
	{
		cout << vec[i].first << " " << balls[i] << endl;
	}
}

void olympiads::TASKS_BY_SOLUTIONS(const map <string, vector <pair<int, double> > > & students, int N)
{
	map <int, int> count;
	for (int j = 1; j <= N; j++)
	{
		int sum = 0;
		for (auto it : students)
		{
			for (int i = 0; i < it.second.size(); i++)
			{
				if (it.second[i].first == j)
					sum++;
			}
		}
		count[j] = sum;
	}
	vector <pair <int, int> > countVec(count.begin(), count.end());
	sort(countVec.begin(), countVec.end(), [](pair <int, int> a, pair <int, int> b) {return a.second < b.second; });
	for (auto it : countVec)
	{
		cout << it.first << " " << it.second << endl;
	}
}

void olympiads::solution(map <string, vector <pair<int, double> > > & students, int N, const vector <double> & bals)
{
	string command;
	cin >> command;
	while (command != "EXIT")
	{
		if (command == "SOLVED")
		{
			SOLVED(students, bals);
		}
		if (command == "STUD_STAT")
		{
			string family;
			cin >> family;
			STUD_STAT(family, students);
		}
		if (command == "STUDS_BY_TASKS")
		{
			STUDS_BY_TASKS(students);
		}
		if (command == "STUDS_BY_BALLS")
		{
			STUDS_BY_BALLS(students);
		}
		if (command == "STUDS_MORE_TASKS")
		{
			STUDS_MORE_TASKS(students);
		}
		if (command == "STUDS_MORE_BALLS")
		{
			STUDS_MORE_BALLS(students);
		}
		if (command == "TASKS_BY_SOLUTIONS")
		{
			TASKS_BY_SOLUTIONS(students, N);
		}
		cin >> command;
	}
}


int main()
{
	int N;
	cin >> N;
	shared_ptr <olympiads> MosOlymp2016;
	map <string, vector <pair<int, double> > > students;
	MosOlymp2016->solution(students, N, MosOlymp2016->getBals(N));
	return 0;
}

