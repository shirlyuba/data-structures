#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <algorithm>
#include <memory>

using namespace std;

class competition
{
public:
	void REGISTER(map <string, int> & participants);
	void VIP(map <string, pair <int, int> > & fans);
	void VOTE(map <string, int> & participants, map <string, pair <int, int> > & fans);
	void KICK(map <string, int> & participants);
	void TOP(map <string, int> & participants);
	void solution(map <string, int> & participants, map <string, pair <int, int> > & fans);
};

void competition::REGISTER(map <string, int> & participants)
{
	string name;
	cin >> name;
	if (participants.find(name) == participants.end())
	{
		participants[name] = 0;
		cout << "OK" << endl;
	}
	else
		cout << "ALREADY REGISTER" << endl;
}

void competition::VIP(map <string, pair <int, int> > & fans)
{
	string phone;
	cin >> phone;
	if (fans[phone].first == 1)
		cout << "ALREADY VIP" << endl;
	else
	{
		fans[phone].first = 1;
		fans[phone].second = 3;
		cout << "OK" << endl;
	}
}

void competition::VOTE(map <string, int> & participants, map <string, pair <int, int> > & fans)
{
	string name, phone;
	cin >> name >> phone;
	if (participants.find(name) == participants.end())
		cout << "NOT REGISTER" << endl;
	else
	{
		if (fans.find(phone) == fans.end())
		{
			fans[phone].second = 1;
		}
		if (fans[phone].second == 0)
			cout << "NO MORE VOTES";
		else
		{
			fans[phone].second--;
			participants[name]++;
			cout << participants[name] << endl;
		}
	}
}

void competition::KICK(map <string, int> & participants)
{
	string name;
	cin >> name;
	if (participants.find(name) == participants.end())
		cout << "NOT REGISTER" << endl;
	else
	{
		participants.erase(name);
		cout << "OK" << endl;
	}
}

void competition::TOP(map <string, int> & participants)
{
	int votes;
	cin >> votes;
	if (votes > 100)
	{
		cout << "wrong numer of votes";
	}
	if (participants.empty())
	{
		cout << "NO PARTICIPANTS" << endl;
	}
	vector <pair <string, int> > partVec(participants.begin(), participants.end());
	sort(partVec.begin(), partVec.end(), [](pair <string, int> a, pair <string, int> b) { return a.second > b.second; });
	for (int i = 0; i < votes; i++)
	{
		if (i == partVec.size())
			return;
		cout << partVec[i].first << " " << partVec[i].second << endl;
	}
}


void competition::solution(map <string, int> & participants, map <string, pair <int, int> > & fans)
{
	string command;
	cin >> command;
	while (command != "EXIT")
	{
		if (command == "REGISTER")
		{
			REGISTER(participants);
		}
		if (command == "VIP")
		{
			VIP(fans);
		}
		if (command == "VOTE")
		{
			VOTE(participants, fans);
		}
		if (command == "KICK")
		{
			KICK(participants);
		}
		if (command == "TOP")
		{
			TOP(participants);
		}
		cin >> command;
	}
	cout << "OK" << endl;
}


int main()
{
	shared_ptr <competition> RussianBeaty2016;
	map <string, int> participants;
	map <string, pair <int, int> > fans;
	RussianBeaty2016->solution(participants, fans);
	return 0;
}

