#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <iterator>
#include <cassert>

using namespace std;

int MAX = 1000;

int main()
{
	vector <int> original;
	original.resize(MAX);
	srand(time(NULL));
	generate(original.begin(), original.end(), []() {return rand() % 1000; }); // 1
	vector <int> copyOfOriginal;
	copy(original.begin(), original.end(), back_inserter(copyOfOriginal)); // 2
	cout << endl;
	random_shuffle(copyOfOriginal.begin(), copyOfOriginal.end()); // 3
	sort(copyOfOriginal.begin(), copyOfOriginal.end(), [](int a, int b) { return a > b; }); // 4
	reverse(copyOfOriginal.begin(), copyOfOriginal.end()); // 5
	assert(copyOfOriginal[0] == *(min_element(copyOfOriginal.begin(), copyOfOriginal.end()))); // 6
	assert(copyOfOriginal.back() == *(max_element(copyOfOriginal.begin(), copyOfOriginal.end()))); // 7
	vector <int> permutation;
	permutation.resize(MAX);
	int t = 0;
	for_each(permutation.begin(), permutation.end(), [&t](int& k) { // 8
		k = t++;
		return k; });
	sort(permutation.begin(), permutation.end(), [&original](int a, int b) { // 9
		return original[a] < original[b]; });
	vector <int> sorted;
	transform(permutation.begin(), permutation.end(), back_inserter(sorted), [&original](int a) { return original[a]; }); // 10
	assert(is_sorted(sorted.begin(), sorted.end())); // 11
	assert(equal(sorted.begin(), sorted.end(), copyOfOriginal.begin())); // 12
	vector <int> counts;
	counts.resize(MAX);
	int x = 0;
	generate(counts.begin(), counts.end(), [&x, &copyOfOriginal]() {
		int i = count(copyOfOriginal.begin(), copyOfOriginal.end(), x);
		assert(i == upper_bound(copyOfOriginal.begin(), copyOfOriginal.end(), x) - lower_bound(copyOfOriginal.begin(), copyOfOriginal.end(), x));
		x++;
		return i; }); // 13, 14
	int sum = 0;
	for_each(counts.begin(), counts.end(), [&sum](int i) { sum += i; }); // 15
	assert(sum = MAX);
	for_each(copyOfOriginal.begin(), copyOfOriginal.end(), [](int a) { cout << a << "  "; }); // 16
	return 0;
}

